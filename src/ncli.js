/**
 * @fileoverview This is the entry point for the NCLI bin script.
 * Takes input module/script to run, where script is resolved as
 * a file in the scripts folder.
 */
const boxen = require('boxen');
const chalk = require('chalk');
const fs = require('fs');
const inquirer = require('inquirer');
const inquirerBaseUI = require('inquirer/lib/ui/baseUI');
// Short-circuit the inquirer force close path.
inquirerBaseUI.prototype.onForceClose = () => {};
const NCLICore = require(__dirname + '/core/core.class.js');
// const NCLIServer = require(__dirname + '/server/server.class.js');

/**
 * The NCLIBinary class bootstraps a console session.  This binary allows for 
 * other files to hook into the menu's because each menu emits a signal to 
 * allow other menu options to be plugged in.  Furthermore, other classes can
 * listen to the feedback as it comes in.
 * @class
 */
class NCLIBinary {
    constructor() {
        if (NCLIBinary.instance) {
            return NCLIBinary.instance;
        } 
        NCLIBinary.instance = this;

        this.cache = NCLICore.cache.get('ncli');
        if (!this.cache) {
            this.initializeCache();
        }
        this.argv = Object.assign({}, NCLICore.argv);
        this.argv._ = this.argv._.slice(2);
        if (this.argv._.length == 0) {
            NCLIBinary.showMainMenu();
        } else {
            process.emit(`NCLIBinary.onStart`, this.argv);
        }
    }

    /**
     * Emits an answer response from an inquirer prompt to the process.
     */
    static emitAnswer(answer) {
        process.emit(`NCLIBinary.menu.onSelect`, {
            answer
        });
        for (let menu in answer) {
            process.emit(`NCLIBinary.menu.onSelect.${menu}`, {
                value: answer[menu]
            });
            process.emit(`NCLIBinary.menu.onSelect.${menu}.${answer[menu]}`, {
                value: null
            });
        }
    }

    /**
     * Attempts to contact the registration server with the credentials stored 
     * locally.  Upon a successful verification of the registrant, resolves 
     * the returned promise with the user.  
     */
    async getRegistration() {

    }

    /**
     * Initializes the cache to include the local NCLI filesystem in the 
     * remembered packages list.  Also initializes keys for most recent scripts
     * that have been run in addition to top scripts that have been run.
     */
    initializeCache() {
        NCLICore.cache.set('ncli', {
            recentScripts: [],
            mostUsedScripts: [],
            savedDirectories: [__dirname]
        });
    }

    /**
     * Prior to bootstrap, attempt to remember all directories and bootstrap 
     * everything.
     */
    rememberDirectories() {
        
    }

    /**
     * Runs a command from within the CLI.
     */
    static runCommand(args) {
        switch (args['_'][0]) {
            case 'configs':
                const configs = NCLICore.getConfigs();
                if (args['_'].length == 1) {
                    NCLIBinary.showConfigsMain();
                } else {
                    switch (args['_'][1]) {
                        case 'show':
                            if (args['_'].length < 3) {
                                console.warn('Please supply a key name.');
                                console._info('\nUsage: ncli configs show <KEY_NAME>\n');
                                break;
                            }
                            if (!configs[args['_'][2]]) {
                                console.warn(`Key "${args['_'][2]}" not found.`);
                            } else {
                                console._log(configs[args['_'][2]]);
                            }
                        break;

                        case 'set': 
                            // Sets a config to the desired value.
                        break;
                    }
                }
            break;
            case 'run': 
                // This should run scripts.
            break;
            case 'forget': 
                // This should forget this directory for its scripts and NCLI configs.
            break;
            case 'remember':
                // This should remember this directory for its scripts and NCLI configs.
                // NCLICore.cache.
                let ncliCache = NCLICore.cache.get('ncli');
                ncliCache.savedDirectories.push(process.cwd());
                NCLICore.cache.set('')
            break;
        }
        process.exit();
    }

    /**
     * Shows the NCLI title box.
     */
    static showTitle() {
        let versionNumber = NCLICore.packages
            .getPackageObject(__dirname)['version'];
        let version = `Version ${versionNumber.length % 2 == 0 ? '' : ' '}` + 
            `${versionNumber}`;
        let title = '';
        for (let i = 0; i < Math.floor(version.length / 2) - 
            NCLICore.localConfigs.ncli.title.length / 2; i++) {
            title += ' ';
        }
        title += chalk.green(NCLICore.localConfigs.ncli.title) + '\n' + 
            chalk.grey(version);
        console._log(boxen(title, {float: 'center', padding: 1}));
    }

    /**
     * Shows the main menu with all of the options available to a user.
     */
    static showMainMenu() {
        NCLIBinary.showTitle();
        let choices = [
            {
                name: `Scripts ${chalk.grey('-- View or Run')}`,
                value: 'scripts'
            },
            {
                name: `Packages ${chalk.grey('-- View or Change')}`,
                value: 'packages'
            },
            {
                name: `Configs ${chalk.grey('-- View')}`,
                value: 'configs'
            },
            new inquirer.Separator(),
            {
                name: `NCLI Docs ${chalk.grey('-- View')}`,
                value: 'docs'
            },
            {
                name: `NCLI News ${chalk.grey('-- View')}`,
                value: 'news'
            },
            {
                name: `NCLI Help ${chalk.grey('-- View')}`,
                value: 'help'
            },
            new inquirer.Separator()
        ];
        process.emit('NCLI.showMainMenu.getChoices', choices);
        inquirer.prompt([{
            type: 'list',
            name: 'main',
            default: choices[0]['value'] || choices[0]['name'],
            message: 'Select from the following options:',
            choices
        }]).then(NCLIBinary.emitAnswer);
    }

    /**
     * Shows the main menu for packages.
     */
    static showPackagesMain() {
        inquirer.prompt({
            type: 'list',
            name: 'packagesMain',
            message: 'What would you like to do?',
            choices: [{
                name: 'View Packages Table',
                value: 'table'
            }, {
                name: 'View Packages List',
                value: 'list'
            }]
        }).then(NCLIBinary.emitAnswer);
    }

    /**
     * Shows all of the packages as queried.
     */
    static showPackages() {
        const packages = NCLICore.getPackages();
        const choices = [];
        for (let pkg of packages) {
            let relDir = '';
            if (pkg['relativeDir'].length > 0) {
                relDir = chalk.grey(`(${pkg['relativeDir']})`);
            }
            choices.push({
                name: `${pkg['name']}${relDir}`,
                value: pkg['name']
            });
        }
        // NCLICore.saySomething(`Found ${packages.length} packages`);
        inquirer.prompt({
            type: 'list',
            name: 'packagesList',
            message: `Found ${packages.length} packages.  Select a package...`,
            choices
        }).then(NCLIBinary.emitAnswer);
    }

    /**
     * Shows options for an individual package.  This includes both viewing 
     * configs and scripts.
     * @param {*} pkg 
     */
    static showPackage(pkg) {

    }

    /**
     * Shows the packages as menu options.
     */
    static showAllPackageScripts() {
        const scripts = NCLICore.getScripts();
        let numScripts = 0;
        const choices = [];
        for (let pkg in scripts) {
            const numPkgScripts = Object.keys(scripts[pkg]['scripts']).length;
            numScripts += numPkgScripts;
            choices.push({
                name: `${pkg} (${numPkgScripts} scripts)`,
                value: pkg
            })
        }

        /*
        NCLICore
            .saySomething(`Found ${numScripts} scripts across ${Object.keys(scripts)
            .length} packages`);
        */
        inquirer.prompt({
            type: 'list',
            name: 'scriptsByPackage',
            message: `Found ${Object.keys(scripts).length} packages.  Select a package...`,
            choices
        }).then(NCLIBinary.emitAnswer);
    }

    /**
     * Prints out a table of all packages which includs the scripts and configs
     * count.
     */
    static showPackagesTable() {

    }

    /**
     * Shows all the scripts for the selected package.
     */
    static showPackageScripts() {

    }

    /**
     * Shows the registration screen for the user.
     */
    static showRegistration() {
        NCLICore
            .saySomething(`Welcome`);
    }

    /**
     * Shows scripts via the main menu.  In general, scripts may be run in four
     * different ways:
     *   - most popular
     *   - most recent
     *   - by packages
     *   - via user entry
     */
    static showScriptsMain() {
        inquirer.prompt({
            type: 'list',
            name: 'scriptsMain',
            message: 'View or Run a Script...',
            choices: [{
                name: 'By Most Popular',
                value: 'popular'
            }, {
                name: 'By Most Recent',
                value: 'recent'
            }, {
                name: 'By Package',
                value: 'packages'
            }, {
                name: 'By Entry',
                value: 'entry'
            }]
        }).then(NCLIBinary.emitAnswer);
    }
}

// process.on('NCLIBinary.onStart', NCLIBinary.runCommand);

process.on('NCLIBinary.menu.onSelect.main.packages', NCLIBinary.showPackagesMain);
process.on('NCLIBinary.menu.onSelect.packagesMain.list', NCLIBinary.showPackages);
process.on('NCLIBinary.menu.onSelect.packagesMain.table', NCLIBinary.showPackagesTable);

// Scripts...
process.on('NCLIBinary.menu.onSelect.main.scripts', NCLIBinary.showScriptsMain);
process.on('NCLIBinary.menu.onSelect.scriptsByPackage', NCLIBinary.showPackageScripts);
process.on('NCLIBinary.menu.onSelect.scriptsMain.packages', NCLIBinary.showAllPackageScripts);
new NCLIBinary();
