const Cache = require('ncli-core-cache/cache.class.js');
const Configs = require('ncli-core-configs/configs.class.js');
const FileHelpers = require('ncli-core-files/filehelpers.class.js');
const FileScanner = require('ncli-core-files/filescanner.class.js');
const FileWriter = require('ncli-core-files/filewriter.class.js');
const fs = require('fs');
const minimist = require('minimist')(process.argv);
const Packages = require('ncli-core-files/packages.class.js');
const RScandir = require('ncli-core-rscandir');

/**
 * @class
 * Cache class for setting the caches relevant to core NCLI package.
 * 
 * The following cache stores are handled:
 *   - packageObjects
 *   - nodeModulesPackages
 *   - localPackages
 *   - fileFolders
 *   - nodeModulesFiles
 *   - localFiles
 * 
 * Of all the cache stores, each one persists but has different behavior 
 * between `dev` and `prod`.
 * 
 * Many of these cache stores shall be flushed and re-read on install/update.
 * 
 * The way this is all achieved is to listen on startup and for dev mode to 
 * store the cache, restore it (and enrich it) on shutdown, but to read freshly
 * and only from in memory during runtime.
 */
class CoreCache { 
    constructor() {
        if (CoreCache.instance) {
            return CoreCache.instance;
        }
        CoreCache.instance = this;

        process.on('NCLICore.preBootstrap', CoreCache.preBootstrap);
        process.on('NCLICore.bootstrap', CoreCache.bootstrap);
    }

    /**
     * Bootstraps the Cache onto the underlying packages that require a cache 
     * or are benefitted by the cache.
     */
    static bootstrap() {
        // Ensure cache is accessible on objects needing it.
        if (minimist.noCache || minimist['no-cache'] === 'false') {
            return;
        }
        
        if (!minimist.noCache && !minimist['no-cache']) {
            RScandir.Cache = Cache;
            FileScanner.Cache = Cache;
            Packages.Cache = Cache;
        }

        let packagesCache = Packages.localCache;
        let filesCache = FileScanner.localCache;

        // Retrieve any existing cache objects from simple cache prior to load.
        const transferCache = (key, srcObj, simulate = false) => {
            if (!Cache.has(key)) {
                new Cache(key, {}, {persist: true});
            }
            if (!srcObj[key]) {
                srcObj[key] = {};
            }
            Cache.objects[key].value = Configs
                .deepAssign(Cache.objects[key].value || {}, srcObj[key]);
        }

        transferCache('localFiles', filesCache);
        transferCache('nodeModulesFiles', filesCache);
        // transferCache('fileFolders', filesCache);
        transferCache('localPackages', packagesCache);
        transferCache('nodeModulesPackages', packagesCache);
        transferCache('packageObjects', packagesCache);

        process.on('Cache.save', (objects) => {
            const name = Packages.getPackageObject(FileHelpers
                .getPackageFile(process.cwd()))['name'];
            const base = `${__dirname}/.packageCache/${name}/`;
            try {
                FileWriter.writeFile(`${base}/packageObjects.json`,
                    JSON.stringify(Cache.objects['packageObjects']), true);
                FileWriter.writeFile(`${base}/nodeModulesPackages.json`,
                    JSON.stringify(Cache.objects['nodeModulesPackages']), true);
                FileWriter.writeFile(`${base}/localPackages.json`,
                    JSON.stringify(Cache.objects['localPackages']), true);
                // FileWriter.writeFile(`${base}/fileFolders.json`,
                //    JSON.stringify(Cache.objects['fileFolders']));
                FileWriter.writeFile(`${base}/nodeModulesFiles.json`,
                    JSON.stringify(Cache.objects['nodeModulesFiles']), true);
                FileWriter.writeFile(`${base}/localFiles.json`,
                    JSON.stringify(Cache.objects['localFiles']), true);
            } catch(e) {console.error(e)}
        });

        // Reset the temporary cache.
        FileScanner.localCache = {};
        Packages.localCache = {};
    }

    /**
     * Bootstraps a dev environment.
     */
    static bootstrapDev() {
        const envObj = {};
        process.emit('environment', envObj);
        if (envObj['value'] == 'dev') {
            // Detach localPackages, all packageObjects that are local.
            const localPackages = Configs.deepAssign({}, 
                Cache.objects['localPackages'].value);
            Cache.objects['localPackages'].value = {};
            process.on('Cache.save', (objects) => {
                // Restore localPackages on exit and override with local changes.
                objects['localPackages'].value = Configs
                    .deepAssign(localPackages, objects['localPackages'].value);
            });
            
            const packageObjects = Cache.objects['packageObjects'];
            // Cache exists.
            if (packageObjects.value) {
                for (let path in packageObjects.value) {
                    try {
                        // Delete only if file stat shows more recent modtime.
                        if (fs.statSync(path).mtime > packageObjects.lastModified) {
                            delete packageObjects.value[path];
                        }
                    } catch(e) {delete packageObjects.value[path];}
                }
            }
            
            process.on('Cache.save', (objects) => {
                objects['packageObjects'].value = Configs
                    .deepAssign(packageObjects.value, objects['packageObjects'].value);
            });

            // TODO: localFiles.
            // localFiles ...
            // ...
        }
    }

    /**
     * Flushes all caches and rebuilds.  Performed on install and on update.
     */
    static flush() {

    }

    /**
     * Bootstraps the necessary files.  What this does is it ensures the 
     * availability of the cache from the filesystem.  Without this step, the 
     * bootstrapping of NodeClient would trigger a filesystem read thereby 
     * eliminating the utility of the filesystem cache.
     */
    static preBootstrap() {
        const name = Packages.getPackageObject(FileHelpers
            .getPackageFile(process.cwd()))['name'];
        const base = `${__dirname}/.packageCache/${name}/`;
        if (!fs.existsSync(base)) {
            return;
        }
        let packagesCache = Packages.localCache || {};
        let filesCache = FileScanner.localCache || {};
        packagesCache['packageObjects'] = Cache
            .preRead(`${base}/packageObjects.json`);
        packagesCache['nodeModulesPackages'] = Cache
            .preRead(`${base}/nodeModulesPackages.json`);
        packagesCache['localPackages'] = Cache
            .preRead(`${base}/localPackages.json`);
        // filesCache['fileFolders'] = Cache
        //    .preRead(`${base}/fileFolders.json`);
        filesCache['nodeModulesFiles'] = Cache
            .preRead(`${base}/nodeModulesFiles.json`);
        filesCache['localFiles'] = Cache
            .preRead(`${base}/localFiles.json`);
    }
}

new CoreCache();
module.exports = CoreCache;