// const Cache = require(__dirname + '/../cache.class.js');
// const Files = require('ncli-core-files').Files;
// const RScandir = require('ncli-core-rscandir');
xdescribe('RScandir Cache', () => {
    it('Should return files from the RScandir cache.', () => {
        let cacheObj = {};
        cacheObj[Files.normalize(__dirname + '/../foo')] = ['foo', 'bar'];
        Cache.set('rscandir', cacheObj, {'persist': false});
        Files.writeFile(__dirname + '/../foo/barrish.json', 
            'foo', true, 'utf8', true);
        let result = RScandir(__dirname + '/../foo');
        expect(result[0]).not.toBe('tests/../foo/barrish.json');
        expect(result[0]).toBe('foo');
        Cache.clear(null, true);
        result = RScandir(__dirname + '/../foo');
        expect(result[0]).toBe('foo/barrish.json');
    });

    it('Should utilize numFilesThreshold and recursionThreshold.', () => {

    });
});