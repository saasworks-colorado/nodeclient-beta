const childProcess = require('child_process');
const fs = require('fs');
const NCLICore = require(__dirname + '/../core.class.js');

describe('NCLICore', () => {
    describe('getEnv', () => {
        beforeEach(() => {
            NCLICore.files.writeFile(__dirname + '/blah.js', `
const NCLICore = require(__dirname + '/../core.class.js');
console.log('######', NCLICore.getEnv(), '######');
`, false, 'utf8', true);
        });
        it('Should properly detect the environment.', () => {
            expect(NCLICore.getEnv()).toBe('test'); // We're in a test runner, derp!
            // Ensures that the default env is 'dev'
            expect(childProcess.execSync('node tests/blah.js', {
                cwd: __dirname + '/../'
            }).toString('utf8').trim().split(/\#{6}/g)[1].trim()).toBe('dev');
            // Ensures that with CLI args that env is read from CLI.
            const env = NCLICore.argv['env'];
            NCLICore.argv['env'] = 'prod';
            expect(NCLICore.getEnv()).toBe('prod');
            NCLICore.argv['env'] = env;
        });
        afterEach(() => {
            try {fs.unlinkSync(__dirname + '/blah.js');} catch(e) {}
        });
    });

    it('Should allow all package versions to be managed from the CLI.', () => {

    });
});