const helpers = require(__dirname + '/test.helpers.js');
const NCLICore = require(__dirname + '/../core.class.js');

describe('Filesystem', () => {
    let cwd;
    beforeEach(async () => {
        cwd = process.cwd();
        await helpers.createFiles();
        NCLICore.getPackages();
    });

    it('Should get a file listing.', () => {
        let results = NCLICore.getFiles('sql');
        expect(results.length).toBe(5);
        results.map((file) => {
            expect(file.includes('data/sql/')).toBe(true);
        });
        results = NCLICore.getFiles('sql', true);
        expect(Object.keys(results).length).toBe(1);
        expect(results['chatbot-data'].length).toBe(5);
    });

    it('Should get file contents.', () => {
        let results = NCLICore.getFileContents('logs');
        expect(Object.keys(results).length).toBe(5);
        for (let file in results) {
            expect(require('fs').readFileSync(file, 'utf8'))
                .toBe(results[file]);
        }
        results = NCLICore.getFileContents('logs', true);
        expect(Object.keys(results).length).toBe(4);
    });

    it('Should allow files to be required.', () => {
        NCLICore.requireFiles('actions', (exports, pkg, file) => {
            const filename = file.split(/\//g).pop().split(/\./g)
                .slice(0, -1)[0];
            expect(filename).toBe(exports.name());
        });
    });

    it('Should error handle required files properly.', () => {
        let failureCount = 0;
        let successCount = 0;
        NCLICore.requireFiles('actions-with-error', 
            (exports, pkg, file) => successCount++,
            (err, pkg, file) => failureCount++);
        expect(failureCount).toBe(1);
        expect(successCount).toBe(1);
    });

    it('Should properly read JSON.', () => {
        let results = NCLICore.getJSON('configs');
        expect(Object.keys(results).length).toBeGreaterThan(2);
        expect(results.hasOwnProperty('chatbot')).toBe(true);
        expect(results['chatbot']['httpPort']).toBe(81);
        expect(results['chatbot']['db']['user']).toBe('root');
        expect(results['chatbot']['crawl']['src'].includes('amazon')).toBe(true);
        expect(results['chatbot']['api'].includes('.com')).toBe(true);
        process.chdir('chatbot');
        results = NCLICore.getJSON('configs');
        expect(Object.keys(results).length).toBe(2);
    });

    afterEach(() => {
        process.chdir(cwd);
        helpers.cleanup();
    });
});