const fs = require('fs');
const NCLICore = require(__dirname + '/../core.class.js');

/**
 * @class Rigs up a variety of local packages with various seed data.
 */
class NCLITestHelpers {
    constructor() {
    }

    /**
     * Cleans up all local test files.
     */
    cleanup() {
        NCLICore.files.rrmdir(__dirname + '/../chatbot');
    }

    /**
     * Creates all necessary local files for testing.
     */
    async createFiles() {
       await this.createLocalPackages();
    }

    /**
     * This migrates the .seed folder to the main directory.
     */
    async createLocalPackages() {
        await NCLICore.files.copyFiles(__dirname + '/.seed', __dirname + '/../');
    }
}

module.exports = new NCLITestHelpers();