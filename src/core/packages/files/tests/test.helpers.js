class FilesTestHelpers {
    constructor() {
        this.files = require(__dirname + '/../index.js').Files;
    }
    cleanupFiles() {
        this.files.rrmdir('filesFiles');

        try {
            require('fs').unlinkSync('node_modules/foobar');
        } catch(e) {}

        this.files.rrmdir('../foobar');
        this.files.rrmdir('node_modules/filesFilesTestModuleNonPackage');
        this.files.rrmdir('node_modules/filesFilesTestModule');
        this.files.rrmdir('node_modules/otherFilesModule');
    }

    createFiles() {
        this.createLocalPackages();
        this.createLocalFiles();
        this.createNodeModulesPackages();
        this.createNodeModulesFiles();
        this.createExtendedNodeModulesPackages();
        this.createSymlinks();
    }

    createExtendedNodeModulesFiles() {
        this.files.addFiles({
            'filesFiles/subdir/node_modules/foobar/filesFiles/a.json': '{"a": "0.1.2"}',
            'filesFiles/subdir/node_modules/foobar2/filesFiles/a.json': '{"a": "0.1.4"}',
            'filesFiles/subdir/node_modules/foo/node_modules/foobar/filesFiles/a.json': '{"a": "0.2.2"}',
            'filesFiles/subdir/node_modules/foo/node_modules/foobar2/filesFiles/a.json': '{"a": "0.1.4"}',
        });
    }

    createExtendedNodeModulesPackages() {
        this.files.addFiles({
            // subpackage1
            'filesFiles/subdir/node_modules/foobar/package.json': '{"name": "subpackage1", "version": "0.1.2"}',
            // subpackage3
            'filesFiles/subdir/node_modules/foobar2/package.json': '{"name": "subpackage3", "version": "0.1.4"}',
            // subpackage2
            'filesFiles/subdir/node_modules/foo/package.json': '{"name": "subpackage2", "version": "0.0.2"}',
            // subpackage1
            'filesFiles/subdir/node_modules/foo/node_modules/foobar/package.json': '{"name": "subpackage1", "version": "0.2.2"}',
            // subpackage3
            'filesFiles/subdir/node_modules/foo/node_modules/foobar2/package.json': '{"name": "subpackage3", "version": "0.1.4"}'
        });
    }

    createLocalFiles() {
        this.files.addFiles({
            'filesFiles/atest.json': '{"a": "b"}',
            'filesFiles/ztest.json': '{"a": "b"}',
            'filesFiles/subdir/atest.json': '{"a": "b"}',
            'filesFiles/subdir2/atest.json': '{"a": "b"}',
            'filesFiles/subdir2/atest.json': '{"a": "b"}'
        });

        // These files help with the FileScanner.
        this.files.addFiles({
            'filesFiles/subdir/www/atest.txt': 'foobar',
            'filesFiles/subdir/www/btest.txt': 'foo',
            'filesFiles/subdir2/www/ctest.txt': 'bar',
            'filesFiles/subdir2/www/dtest.txt': 'baz',
            'filesFiles/subdir2/www/ctest.txt': 'bar',
            'filesFiles/subdir2/www/dtest.txt': 'baz',
            'filesFiles/subdir/subdir/www/etest.txt': '1'
        });
    }

    createLocalPackages() {
        this.files.addFiles({
            // package1
            'filesFiles/subdir/package.json': '{"name": "package1", "version": "0.1.2"}',
            // package7
            'filesFiles/subdir/subdir/package.json': '{"name": "package7"}',
            // package1
            'filesFiles/subdir2/package.json': '{"name": "package1", "version": "0.1.3"}',
            // package1
            'filesFiles/subdir/subdir3/package.json': '{"name": "package1", "version": "0.1.3"}',
            // package8
            'filesFiles/asd/package.json': '{"name": "package8", "version": "0.1.3"}',
            // package10
            'filesFiles/asd/zxc/package.json': '{"name": "package10", "version": "0.1.3"}',
            // package9
            'filesFiles/bsd/package.json': '{"name": "package9", "version": "0.1.3"}'
        });
    }

    createNodeModulesFiles() {
        this.files.addFiles({
            'node_modules/package6/www/atest.txt': 'foo',
            'node_modules/package6/www/btest.txt': 'bar',
            'node_modules/filesFilesTestModule/www/ctest.txt': 'bar',
            'node_modules/filesFilesTestModule/subdir/www/dtest.txt': 'foobar',
            'node_modules/filesFilesTestModule/node_modules/nestedFolder/www/etest.txt': 'foobaz',
        });
    }

    createNodeModulesPackages() {
        this.files.addFiles({
            // package5
            'node_modules/filesFilesTestModule/subdir/package.json':
                '{"name": "package5"}',
            // package6
            'node_modules/filesFilesTestModule/node_modules/nestedFolder/package.json':
                '{"name": "package6"}',
            // package4
            'node_modules/filesFilesTestModule/package.json':
                '{"name": "package4"}',
            // package2
            'node_modules/filesFilesTestModuleNonPackage/subdir/package.json':
                '{"name": "package2"}',
            // package6
            'node_modules/package6/package.json':
                '{"name": "package6"}',
            // package3
            'node_modules/filesFilesTestModuleNonPackage/node_modules/abc/package.json':
                '{"name": "package3"}'
        });

        // package4
        this.files.addFiles({
            'node_modules/filesFilesTestModule/filesFiles/atest.json':
                '{"a": "123", "b": "c"}',
            'node_modules/filesFilesTestModule/filesFiles/ztest.json':
                '{"a": "234", "b": "d"}'
        });

        // package6
        this.files.addFiles({
            'node_modules/filesFilesTestModule/node_modules/nestedFolder/filesFiles/atest.json':
                '{"b": "d", "c": "d"}',
            'node_modules/filesFilesTestModule/node_modules/nestedFolder/filesFiles/ztest.json':
                '{"b": "123", "c": "345"}',
            'node_modules/filesFilesTestModule/node_modules/nestedFolder/filesFiles/subdir/ztest.json':
                '{"b": "e", "c": "f"}'
        })

        // No module for this file.
        this.files.addFiles({
            'node_modules/filesFilesTestModuleNonPackage/atest.json':
                '{"a": "123", "b": "c"}',
            'node_modules/filesFilesTestModuleNonPackage/filesFiles/atest.json':
                '{"a": "123", "b": "c"}'
        });

        // package6 Files
        this.files.addFiles({
            'node_modules/package6/filesFiles/atest.json':
                '{"b": "d", "c": "d"}',
            'node_modules/package6/filesFiles/ztest.json':
                '{"b": "123", "c": "345"}',
            'node_modules/package6/filesFiles/subdir/ztest.json':
                '{"b": "e", "c": "f"}'
        });
    }

    createSymlinks() {
        this.files.addFiles({
            '../foobar/package.json': '{"name": "package0"}',
            '../foobar/filesFiles/btest.json': '{"a": "b"}',
            '../foobar/www/btest.json': '{"a": "b"}'
        });

        try {
            require('fs').symlinkSync('../../foobar', 
                __dirname + '/../node_modules/foobar',
                'dir');
            return;
            require('child_process')
                .execSync('ln -s -d "../../foobar" "./"', {
                    stdio: ['pipe', 'pipe', 'pipe'],
                    cwd: __dirname + '/../node_modules'
                });
        } catch(e) {}
    }
}
module.exports = new FilesTestHelpers();