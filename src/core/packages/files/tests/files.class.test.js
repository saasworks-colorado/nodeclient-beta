xdescribe('Files Class', function() {
    let cwd, files, fs, f, helpers, rscandir, rimraf;
    beforeEach(() => {
        cwd = process.cwd();
        process.chdir(__dirname + '/../');
        files = require(process.cwd() + '/index.js');
        fs = require('fs');
        helpers = require(__dirname + '/test.helpers.js');
        helpers.createFiles();
        rimraf = require('rimraf');
        rscandir = require('ncli-core-rscandir');
    });

    it('Should read files from the specified directory based on the current ' +
        'working directory.', () => {
        let arr, i;
        f = files.readLocalFiles('filesFiles');
        arr = [];
        for (i in f) {
            expect(fs.existsSync(f[i])).toBe(true);
            arr.push(JSON.parse(fs.readFileSync(f[i])));
        }
        expect(arr[0]['a']).toBe('b');
    });

    it('Should read files from the specified directory in the ' +
        'current working directory and all node_modules packages.', () => {
        let arr, i;
        f = files.readAllFiles('filesFiles');
        arr = [];
        for (i in f) {
            expect(fs.existsSync(f[i])).toBe(true);
            arr.push(JSON.parse(fs.readFileSync(f[i])));
        }
        expect(arr[0]['b']).toBe('e');
        expect(arr[1]['b']).toBe('d');
        expect(arr[2]['b']).toBe('123');
        expect(arr[5].hasOwnProperty('b')).toBe(false);
    });

    it('Should read files from the specified directory in the ' +
        'current working directory, all node_modules packages, and all ' + 
        'subdirectories that are packages.', () => {
        });

    it('Should flatten files successfully.', () => {
        
    });

    it('Should propagate require overrides into the files object.', () => {

    });


    afterEach(function() {
        helpers.cleanupFiles();
        process.chdir(cwd);
    });
});