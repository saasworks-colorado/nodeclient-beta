const FileHelpers = require(__dirname + '/../filehelpers.class.js');
const Packages = require(__dirname + '/../packages.class.js');

describe('Packages', () => {
    let cwd, files, fs, f, helpers, rscandir, rimraf;
    beforeEach(() => {
        cwd = process.cwd();
        process.chdir(__dirname + '/../');
        files = require(process.cwd() + '/index.js');
        fs = require('fs');
        helpers = require(__dirname + '/test.helpers.js');
        helpers.createFiles();
        rimraf = require('rimraf');
        rscandir = require('ncli-core-rscandir');
    });

    // Finds all packages in local filesystem.
    it('Should find all local packages.', () => {
        const localPackages = Packages.getLocalPackages();
        expect(localPackages.length).toBe(8);

        expect(localPackages.find((val, idx) => {
            return val['name'] == 'package7';
        })['name']).toBe('package7');

        expect(localPackages.find((val, idx) => {
            return val['name'] == 'package8';
        })['name']).toBe('package8');

        expect(localPackages.find((val, idx) => {
            return val['name'] == 'package9';
        })['name']).toBe('package9');

        expect(localPackages.find((val, idx) => {
            return val['name'] == 'package10';
        })['name']).toBe('package10');

        expect(localPackages.find((val, idx) => {
            return val['name'] == 'package1';
        })['name']).toBe('package1');

        expect(localPackages.filter((val, idx) => {
            return val['name'] == 'package1';
        }).length).toBe(3);
    });

    // Finds all packages in node_module filesystem.
    it('Should find all node_modules packages.', () => {
        const nodeModulesPackages = Packages.getNodeModulesPackages();
        expect(nodeModulesPackages.find((val, idx) => {
            return val['name'] == 'package4';
        })['name']).toBe('package4');

        expect(nodeModulesPackages.find((val, idx) => {
            return val['name'] == 'package0';
        })['name']).toBe('package0');

        expect(nodeModulesPackages.find((val, idx) => {
            return val['name'] == 'package0';
        })['isSymlink']).toBe(true);
    });

    it('Should return a filtered list of packages by name.', () => {
        const packages = new Packages();
        const filteredPackages = packages
            .filterPackages(/package[0-9]+/g);
        filteredPackages.map(({name}) => {
            expect(name.startsWith('package')).toBe(true);
        })
    });

    // Returns the correct version comparison.
    it('Should properly compare versions.', () => {
        // TODO: This should be migrated to a different package.
        expect(Packages.cmpVersions('0.1.2', '0.3.4')).toBe(-1);
        expect(Packages.cmpVersions('0.3.4', '0.3.4')).toBe(0);
        expect(Packages.cmpVersions('0.3.5', '0.3.4')).toBe(1);
    });
    
    it('Should return local and node_modules packages.', () => {
        const allDetailsPackages = (new Packages(null, {
            detailLevel: Packages.DetailLevel.ALL_DETAILS
        })).exec();

        // All the local packages.
        expect(allDetailsPackages['package1'].isLocal).toBe(true);
        expect(allDetailsPackages['package7'].isLocal).toBe(true);
        expect(allDetailsPackages['package8'].isLocal).toBe(true);
        expect(allDetailsPackages['package9'].isLocal).toBe(true);
        expect(allDetailsPackages['package10'].isLocal).toBe(true);
        
        // All the node_modules packages
        expect(allDetailsPackages['package0'].isLocal).toBe(false);
        expect(allDetailsPackages['package0'].dir.includes('node_modules'))
            .toBe(true);
        expect(allDetailsPackages['package4'].isLocal).toBe(false);
        expect(allDetailsPackages['package4'].dir.includes('node_modules'))
            .toBe(true);
    });

    it('Should properly return detail levels within package queries.', () => {
        const noDetailPackages = (new Packages(null, {
            detailLevel: Packages.DetailLevel.NO_DETAIL
        })).exec();
        const allDetailsPackages = (new Packages(null, {
            detailLevel: Packages.DetailLevel.ALL_DETAILS
        })).exec();
        for (let name in noDetailPackages) {
            const json = JSON
                .parse(require('fs')
                .readFileSync(`${noDetailPackages[name]}/package.json`, 
                'utf8'));
            expect(json['name']).toBe(name);
            expect(allDetailsPackages.hasOwnProperty(name));
            expect(json['name']).toBe(allDetailsPackages[name]['name']);
            expect(json['name']).toBe(name);
        }
    });

    it('Should return proper packages with multiple versions.', () => {
        const cwd = process.cwd();
        process.chdir(__dirname + '/../filesFiles');
        const allDetailsPackages = (new Packages(null, {
            detailLevel: Packages.DetailLevel.NO_DETAIL,
            multipleVersions: true
        })).exec();
        expect(Object.keys(allDetailsPackages['package1']).length)
            .toBeGreaterThan(1);
        expect(allDetailsPackages['package1'].hasOwnProperty('0.1.3'))
            .toBe(true);
    });

    it('Should resolve a package with the shortest path, all things equal.', 
        () => {
            const cwd = process.cwd();
            process.chdir(__dirname + '/../filesFiles');
            const noDetailPackages = (new Packages(null, {
                detailLevel: Packages.DetailLevel.NO_DETAIL
            })).exec();
            expect(noDetailPackages['package1'].split(/\//g).pop())
                .toBe('subdir2');
        });

    describe('Caching', () => {
        it('Should utilize local caching for Packages.getPackageObject.', () => {
            const pkgName = FileHelpers.normalize(__dirname + 
                '/../filesFiles/subdir/package.json');
            Packages.getPackageObject(pkgName);
            expect(Packages.localCache['packageObjects']
                .hasOwnProperty(pkgName)).toBe(true);
            expect(Packages.localCache['packageObjects'][pkgName]['name'])
                .toBe('package1');
            const pkgObj = Object.assign({}, 
                Packages.localCache['packageObjects'][pkgName]);
            pkgObj['name'] = 'package1a';
            require('fs').writeFileSync(pkgName, JSON.stringify(pkgObj), 'utf8');
            expect(Packages.getPackageObject(pkgName)['name']).toBe('package1');
            delete Packages.localCache['packageObjects'];
            expect(Packages.getPackageObject(pkgName)['name']).toBe('package1a');
        });
        
        it('Should utilize local caching for Packages' | 
            '.getNodeModulesPackages.', () => {
                const dirName = FileHelpers.normalize(__dirname + 
                    '/../filesFiles/subdir');
                let pkgs = Packages
                    .getNodeModulesPackages(dirName);
                expect(Packages.localCache['nodeModulesPackages']
                    .hasOwnProperty(dirName));
                expect(Packages.localCache['nodeModulesPackages'][dirName]
                    .find(({name}) => 'subpackage1')).not.toBe(null);
                expect(Packages.localCache['nodeModulesPackages'][dirName]
                    .find(({name}) => 'subpackage2')).not.toBe(null);
                expect(Packages.localCache['nodeModulesPackages'][dirName]
                    .find(({name}) => 'subpackage3')).not.toBe(null);

                const pkgObj = Packages.localCache['nodeModulesPackages'][dirName]
                    .find(({name}) => 'subpackage1');
                
                const dir = `${pkgObj['dir']}/package.json`;
                Packages.localCache['packageObjects']
                    [dir]['name'] = 'package1a';
                pkgs = Packages
                    .getNodeModulesPackages(dirName);
                
                let pkg = pkgs.find(({dir}) => dir == pkgObj['dir']);
                expect(pkg['name']).toBe('subpackage2');

                delete Packages.localCache['nodeModulesPackages'];
                pkgs = Packages
                    .getNodeModulesPackages(dirName);
                
                pkg = pkgs.find(({dir}) => dir == pkgObj['dir']);
                expect(pkg['name']).toBe('package1a');
            });

        it('Should utilize local caching for Packages.getLocalPackages.', () => {
            const dirName = FileHelpers.normalize(__dirname + 
                '/../filesFiles/subdir');
            let pkgs = Packages
                .getLocalPackages(dirName);
            expect(Packages.localCache['localPackages']
                .hasOwnProperty(dirName));
            expect(Packages.localCache['localPackages'][dirName]
                .find(({name}) => 'package7')).not.toBe(null);
            expect(Packages.localCache['localPackages'][dirName]
                .find(({name}) => 'package1')).not.toBe(null);
                
            const pkgObj = Packages.localCache['localPackages'][dirName]
                .find(({name}) => 'package7');
            
            const dir = `${pkgObj['dir']}/package.json`;
            Packages.localCache['packageObjects']
                [dir]['name'] = 'package7a';
            pkgs = Packages
                .getLocalPackages(dirName);
            
            let pkg = pkgs.find(({dir}) => dir == pkgObj['dir']);
            expect(pkg['name']).toBe('package7');

            delete Packages.localCache['localPackages'];
            pkgs = Packages
                .getLocalPackages(dirName);
            
            pkg = pkgs.find(({dir}) => dir == pkgObj['dir']);
            expect(pkg['name']).toBe('package7a');
        });
    });

    afterEach(function() {
        helpers.cleanupFiles();
        Packages.localCache = {};
        process.chdir(cwd);
    });
});