describe('FileWriter', () => {
    beforeEach(() => {
        cwd = process.cwd();
        process.chdir(__dirname + '/../');
        files = require(process.cwd() + '/index.js').Files;
        fs = require('fs');
        helpers = require(__dirname + '/test.helpers.js');
        helpers.createFiles();
        rimraf = require('rimraf');
        rscandir = require('ncli-core-rscandir');
    });

    describe('FileWriter.moveFiles', () => {
        it('Should move files, folders and subdirectories.', (done) => {
            expect(require('fs').existsSync('node_modules/filesFilesTestModule/' + 
                'filesFiles/btest.json')).toBe(false);
            files.moveFiles('node_modules/filesFilesTestModule/' + 
                'filesFiles/atest.json', 'filesFiles/btest.json').then(() => {
                    expect(require('fs').existsSync('filesFiles/btest.json'))
                        .toBe(true);
                    done();
                });
        });

        it('Should move folders with files.', (done) => {
            expect(require('fs').existsSync('filesFiles2'))
                .toBe(false);
            files.moveFiles('filesFiles', 'filesFiles2').then(() => {
                expect(require('fs').existsSync('fileFiles/atest.json'))
                    .toBe(false);
                expect(require('fs').existsSync('filesFiles2/atest.json'))
                    .toBe(true);
                done();
            }).catch((e) => {
                console.log(e);
            });
        });

        it('Should move folders with files subdirectories and their files.', (done) => {
            expect(require('fs').existsSync('filesFiles2')).toBe(false);
            files.moveFiles('filesFiles', 'filesFiles2').then(() => {
                expect(require('fs').existsSync('filesFiles2/subdir/atest.json'))
                    .toBe(true);
                done();
            }).catch((e) => {
                console.log(e);
            });
        });
    });

    it('Should remove temporary files.', () => {
        files.createFile('temporary', 'a/b.json', '', null, true);
        expect(fs.existsSync('temporary/a/b.json')).toBe(true);
        process.emit('files cleanup');
        expect(fs.existsSync('temporary')).toBe(false);
    });

    it('Should create package files.', () => {
        expect(fs.existsSync('node_modules/abc')).toBe(false);
        files.writeFile('node_modules/abc/a.txt', '');
        expect(fs.existsSync('node_modules/abc')).toBe(true);

        expect(fs.existsSync('node_modules/abc/temporary')).toBe(false);
        files.createPackageFile('temporary', 'a.json', 'contents', 'abc');
        expect(fs.existsSync('node_modules/abc/temporary')).toBe(true);

        rimraf.sync('node_modules/abc');
    });

    it('Should segment files according to a chunk size and resynthesize them ' +
        'into one file.', () => {

        });
    
    it('Should shrink the final chunk size to be the remaining number of ' + 
        'bytes in the final, not to the total chunk size.', () => {

        });

    afterEach(function() {
        helpers.cleanupFiles();
        rimraf.sync(__dirname + '/../filesFiles2');
        process.chdir(cwd);
    });
});