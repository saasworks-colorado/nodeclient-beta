const FileHelpers = require(__dirname + '/../filehelpers.class.js');
const FileScanner = require(__dirname + '/../filescanner.class.js');

describe('FileScanner', () => {
    let cwd, files, fs, f, helpers, rscandir, rimraf;
    beforeEach(() => {
        cwd = process.cwd();
        process.chdir(__dirname + '/../');
        files = require(process.cwd() + '/index.js');
        fs = require('fs');
        helpers = require(__dirname + '/test.helpers.js');
        helpers.createFiles();
        rimraf = require('rimraf');
        rscandir = require('ncli-core-rscandir');
    });

    it('Should get a file within a package as it is in node_modules.', () => {
        expect(fs.existsSync('package6/filesFiles/atest.json'))
            .toBe(false);
        expect(FileScanner.getFile('package6/filesFiles/atest.json')).not
            .toBe(false);
        let contents = FileScanner.getFile('package6/filesFiles/atest.json');
        contents = JSON.parse(contents);
        expect(contents.b).toBe('d');
    });

    it('Should get a file within a package as it is in local modules.', () => {
        expect(fs.existsSync('package1/atest.json'))
            .toBe(false);
        expect(FileScanner.getFilename('package1/atest.json')
            .includes('node_modules')).toBe(false);
        expect(FileScanner.getFile('package1/atest.json')).not
            .toBe(false);
    });

    it('Should scan local files.', () => {
        const files = FileScanner.readLocalFiles('www');
        expect(files.length).toBe(3);
        expect(files[0].includes('ctest')).toBe(true);
        expect(files[1].includes('dtest')).toBe(true);
        expect(files[2].includes('etest')).toBe(true);
    });

    it('Should scan local files in a non-compact way.', () => {
        const localFiles = FileScanner.readLocalFiles('www', false, false, 
            false);
        const nodeModulesFiles = FileScanner.readNodeModulesFiles('www', false, false);
        const files = FileScanner.readFiles('www', false, false, false);
        expect(typeof localFiles).toBe('object');
        expect(Array.isArray(localFiles)).toBe(false);
        const compactLocalFiles = FileScanner.readLocalFiles('www');
        expect(Array.isArray(compactLocalFiles)).toBe(true);
        const localFilesLen = Object.keys(localFiles).length;
        let firstPkg = null;
        for (let pkg in nodeModulesFiles) {
            if (nodeModulesFiles[pkg]['order'] === 0) {
                firstPkg = pkg;
            }
        }
        expect(files[firstPkg]['order']).toBe(localFilesLen);
    });

    it('Should return files as absolute paths.', () => {
        const files = FileScanner.readFiles('www');
        files.map((file) => {
            expect(fs.existsSync(file));
            // Note: this test might be flaky across OS (tested on Win32)
            expect(file.includes(process.cwd()));
        });
    });

    it('Should scan node_modules files.', () => {
        const files = FileScanner.readNodeModulesFiles('www');
        expect(files.length).toBe(4);
        expect(files[0].includes('ctest')).toBe(true);
        expect(files[1].includes('btest')).toBe(true);
        expect(files[2].includes('atest')).toBe(true);
        expect(files[3].includes('btest')).toBe(true);
    });

    it('Should properly cache local file scans.', () => {
        // Note: Importantly, cache only turns on locally when in prod.
        const fn = (obj) => {obj['value'] = 'prod'};
        process.on('environment', fn);
        const dirName = FileHelpers.normalize(FileHelpers.getMainDirectory());
        let localFiles = FileScanner.readLocalFiles('www');

        expect(FileScanner.localCache['localFiles']
            .hasOwnProperty(dirName));
        expect(FileScanner.localCache['localFiles'][dirName]['package1']
            ['files'].hasOwnProperty('www')).toBe(true);
        expect(FileScanner.localCache['localFiles'][dirName]['package1']
            ['files']['www'].find((file) => file.includes('foo')))
            .toBe(undefined);
        expect(FileScanner.localCache['localFiles'][dirName]['package1']
            ['files']['www'].find((file) => file.includes('ctest')))
            .not.toBe(undefined);
        
        const package1Files = FileScanner.localCache['localFiles']
            [dirName]['package1']['files']['www'];
        package1Files[0] = 'foobar';
        
        localFiles = FileScanner.readLocalFiles('www');
        expect(localFiles.find((file) => file == 'foobar')).not.toBe(undefined);

        delete FileScanner.localCache['localFiles'];
        localFiles = FileScanner.readLocalFiles('www');
        
        expect(localFiles.find((file) => file == 'foobar')).toBe(undefined);
        process.removeListener('environment', fn);
    });

    it('Should properly cache node_modules file scans.', () => {
        const dirName = FileHelpers.normalize(FileHelpers.getMainDirectory());
        let nodeModulesFiles = FileScanner.readNodeModulesFiles('www');

        expect(FileScanner.localCache['nodeModulesFiles']
            .hasOwnProperty(dirName));
        expect(FileScanner.localCache['nodeModulesFiles'][dirName]['package4']
            ['files'].hasOwnProperty('www')).toBe(true);
        expect(FileScanner.localCache['nodeModulesFiles'][dirName]['package4']
            ['files']['www'].find((file) => file.includes('foo')))
            .toBe(undefined);
        expect(FileScanner.localCache['nodeModulesFiles'][dirName]['package4']
            ['files']['www'].find((file) => file.includes('ctest')))
            .not.toBe(undefined);
        
        const package4Files = FileScanner.localCache['nodeModulesFiles']
            [dirName]['package4']['files']['www'];
        package4Files[0] = 'foobar';
        
        nodeModulesFiles = FileScanner.readNodeModulesFiles('www');
        expect(nodeModulesFiles.find((file) => file == 'foobar')).not.toBe(undefined);

        delete FileScanner.localCache['nodeModulesFiles'];
        nodeModulesFiles = FileScanner.readNodeModulesFiles('www');
        
        expect(nodeModulesFiles.find((file) => file == 'foobar')).toBe(undefined);
    });

    afterEach(() => {
        helpers.cleanupFiles();
        FileScanner.localCache = {};
        process.chdir(cwd);
    });
});