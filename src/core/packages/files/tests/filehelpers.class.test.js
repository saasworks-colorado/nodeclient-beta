const FileHelpers = require(__dirname + '/../filehelpers.class.js');
describe('FileHelprs', () => {
    let cwd, files, fs, f, helpers, rscandir, rimraf;
    beforeEach(() => {
        cwd = process.cwd();
        process.chdir(__dirname + '/../');
        files = require(process.cwd() + '/index.js').Files;
        fs = require('fs');
        helpers = require(__dirname + '/test.helpers.js');
        helpers.createFiles();
        rimraf = require('rimraf');
        rscandir = require('ncli-core-rscandir');
    });
    
    // Identify the most relevant execution directory.
    it('Should find the first non-node_modules folder given a path.', () => {
        let directory = FileHelpers
            .getApplicationDirectory('node_modules/filesFilesTestModule'); 
        expect(directory).toBe(FileHelpers.normalize(__dirname + '/../'));
        process.chdir('node_modules/filesFilesTestModule');
        expect(directory).toBe(FileHelpers.normalize(__dirname + '/../'));
        expect(FileHelpers.getApplicationDirectory())
            .toBe(FileHelpers.normalize(__dirname + '/../'));
    });

    it('Should return the original file executed with Node.js.', () => {
        // Process directory is current working directory.
        expect(FileHelpers.normalize(FileHelpers.getProcessDirectory())
            .includes('jasmine')).toBe(true);
    });

    it('Shoulld get the main directory intelligently.', () => {
        // TODO: What does 'intelligently' mean?
        // FileHelpers.getMainDirectory();
    });

    it('Should recursively remove directories.', () => {
        // FileHelpers.rrmdir();
    });

    it('Should normalize file paths.', () => {
        // FileHelpers.toFilename();
        // FileHelpers.toFilenames();
    });

    it('Should get the file of the calling function.', () => {
        expect(FileHelpers.getCallerFile(2).includes('jasmine')).toBe(true);
        // Foobar.js
        require('fs').writeFileSync(__dirname + '/foobar.js', 
            'module.exports = require(__dirname + `/../FileHelpers.class.js`)' + 
            '.getCallerFile(1);',
            'utf8');
        // Module.js (edge case to name a file module.js)
        require('fs').writeFileSync(__dirname + '/module.js', 
            'module.exports = require(__dirname + `/foobar.js`);',
            'utf8');
        expect(require(__dirname + '/module.js'))
            .toBe(FileHelpers.normalize(__filename));

        require('fs').unlinkSync(__dirname + '/module.js');
        require('fs').unlinkSync(__dirname + '/foobar.js');
    });

    // Get the package name, derp.
    it('Should find the first package.json given a path.', () => {
        const getName = (path) => 
            JSON.parse(require('fs').readFileSync(path, 'utf8'))['name']
        expect(getName(FileHelpers
            .getPackageFile())).toBe('ncli-core-files');
        expect(getName(FileHelpers
            .getPackageFile(__dirname))).toBe('ncli-core-files');
        expect(getName(FileHelpers
            .getPackageFile(__dirname + '/../filesFiles/subdir')))
            .toBe('package1');
        expect(getName(FileHelpers
            .getPackageFile(__dirname + '/../filesFiles/subdir2')))
            .toBe('package1');
    });
    
    afterEach(function() {
        helpers.cleanupFiles();
        process.chdir(cwd);
    });
});