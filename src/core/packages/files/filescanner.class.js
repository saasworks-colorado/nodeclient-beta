const fs = require('fs');

const FileHelpers = require(__dirname + '/filehelpers.class.js');
const Packages = require(__dirname + '/packages.class.js');
const RScandir = require('ncli-core-rscandir');

/**
 * Mocks Node.js filesystem.  Reads files relative to packages.  Is compatible
 * for node_modules packages as well as finding package.json files in a 
 * directory.  Provides strategies for including or excluding packages.
 * @class
 */
class FileScanner {
    /**
     * Clears out both the local Packages cache AND then calls clear on all
     * caches handled by the packages cache.  Note that this merely clears the
     * in-memory cache.
     * @param {Boolean} fromFilesystem If true, deletes from filesystem as well.
     */
    static clearCache(fromFilesystem = false) {
        FileScanner.localCache = {};
        if (FileScanner.Cache.set) {
            FileScanner.Cache.clear('nodeModulesFiles', fromFilesystem);
            FileScanner.Cache.clear('localFiles', fromFilesystem);
        }
    }

    /**
     * Gets a file by resolving a packageName/path format and ensuring
     * the current working directory is at the 'package.json' root.  This allows
     * for files to be gotten from their most relevant path and also allows for
     * path reuse even when a file is available as a package only (via
     * node_modules).
     * @param {String} file The package file to get.
     * @returns {Boolean|String} The filepath or false if no file found.
     */
    static getFilename(file) {
        const cwd = process.cwd();

        file = FileHelpers.relative(file);
        if (!file.includes('/')) {
            return file;
        }

        const packages = (new Packages()).exec();
        const pkg = file.split(/\//g)[0];
        if (!packages.hasOwnProperty(pkg)) {
            return file;
        }

        file = `${packages[pkg]['dir']}/${file.split(/\//g).slice(1).join('/')}`;
        return file;
    }

    /**
     * @returns {String} The contents of the file.
     */
    static getFile(file) {
        file = FileScanner.getFilename(file);
        return fs.existsSync(file) ? fs.readFileSync(file, 'utf8') : false;
    }

    /**
     * @returns {Boolean} True if fromFolder is specified anywhere in the cache
     * formatted file listing object.
     */
    static hasFolderFiles(fromFolder, cache) {
        return Object.keys(cache).reduce((hasFolderFiles, name) => {
            return hasFolderFiles || 
                (cache[name] && cache[name]['files'][fromFolder]);
        }, false);
    }

    /**
     * Gets files.  Note that this function deliberately destroys references 
     * so as to not impact the underlying cache if/when cache reads occur.
     * @param {String} fromFolder The folder from which to read.
     * @param {boolean} noRead If false, does not rscandir the folders.
     * @param {boolean} noCache If false, does not rscandir the folders.
     * @param {boolean} compact If true, condense the package files into a 
     */
    static readFiles(fromFolder, noRead, noCache, compact = true) {
        let files = FileScanner
            .readNodeModulesFiles(fromFolder, noRead, compact);
        let localFiles = FileScanner
            .readLocalFiles(fromFolder, noRead, noCache, compact);
        if (compact) {
            files = localFiles.concat(files);
        } else {
            files = Object.assign({}, files);
            localFiles = Object.assign({}, localFiles);
            const delta = Object.keys(localFiles).length;
            for (let name in files) {
                files[name] = Object.assign({}, files[name]);
                files[name]['order'] += delta;
            }
            files = Object.assign(files, localFiles);
        }
        return files;
    }
    
    /**
     * Reads files from the fromFolder.  This includes local packages.
     * @param {String} fromFolder The folder from which to read.
     * @param {boolean} noRead If false, does not rscandir the folders.
     * @param {boolean} noCache If false, does not rscandir the folders.
     * @param {boolean} compact If true, condense the package files into a 
     * single array.
     * @return {Object|Array} A listing of files.
     */
    static readLocalFiles(fromFolder, noRead, noCache, compact = true) {
        let envObj = {};
        // TODO: Add a test using `getPackageDirectory` as opposed to `getMainDirectory`
        const cwd = process.cwd();
        const path = FileHelpers.getPackageDirectory(process.cwd());

        // Do cache magic.
        process.emit('environment', envObj);
        if (!envObj['value'] || (noCache === undefined && envObj['value'] == 'dev')) {
            noCache = true;
        }
        if (!noCache && FileScanner.Cache.set) {
            const obj = FileScanner.Cache.get('localFiles');
            if (obj[path] && FileScanner.hasFolderFiles(fromFolder, obj[path])) {
                return compact ? FileScanner.toFilelist(obj[path], fromFolder) : obj[path];
            }
        } else if (!noCache) {
            if (!FileScanner.localCache['localFiles']) {
                FileScanner.localCache['localFiles'] = {};
            }
            const obj = FileScanner.localCache['localFiles'];
            if (obj[path] && FileScanner.hasFolderFiles(fromFolder, obj[path])) {
                return compact ? FileScanner.toFilelist(obj[path], fromFolder) : obj[path];
            }
        }
        
        const localPackages = (new Packages(null, {
            nodeModulesPackages: false
        })).exec();
        const localFiles = {};
        process.chdir(path);
        Object.keys(localPackages).map((name) => {
            localFiles[name] = {
                files: {},
                order: localPackages[name]['order']
            }
            const dir = `${localPackages[name]['dir']}/${fromFolder}`;
            if (fs.existsSync(dir)) {
                if (noRead) {
                    localFiles[name]['files'][fromFolder] = dir;
                } else {
                    localFiles[name]['files'][fromFolder] = 
                        RScandir(dir).map((file) => {
                            const relDir = 
                                localPackages[name]['relativeDir'];
                            let dir = 
                                localPackages[name]['dir'];
                            file = file.substr(relDir.length);
                            // TODO: This is an extra computation that can be avoided.
                            if (!dir.endsWith('/') && !file.startsWith('/')) {
                                dir += '/';
                            } else if (dir.endsWith('/') && file.startsWith('/')) {
                                file = file.substr(1);
                            }
                            return dir + file;
                        });
                }
            }
        });
        process.chdir(cwd);

        if (FileScanner.Cache.set) {
            let obj = FileScanner.Cache.get('localFiles');
            if (!obj) {
                obj = {};
            }
            obj[path] = localFiles;
            FileScanner.Cache.set('localFiles', obj, {
                persist: true
            });
        } else {
            if (!FileScanner.localCache['localFiles']) {
                FileScanner.localCache['localFiles'] = {};
            }
            FileScanner.localCache['localFiles'][path] = localFiles;
        }

        return compact ? FileScanner.toFilelist(localFiles, fromFolder) : localFiles;
    }

    /**
     * Reads files from the fromFolder.
     * @param {String} fromFolder The folder from which to read.
     * @param {boolean} noRead If false, does not rscandir the folders.
     * @param {boolean} compact If true, condense the package files into a 
     * single array.
     * @return {Object|Array} A listing of files.
     */
    static readNodeModulesFiles(fromFolder, noRead, compact = true) {
        // TODO: Add a test using `getPackageDirectory` as opposed to `getMainDirectory`
        const cwd = process.cwd();
        const path = FileHelpers.getPackageDirectory(process.cwd());

        // Do cache magic.
        if (FileScanner.Cache.set) {
            const obj = FileScanner.Cache.get('nodeModulesFiles');
            if (obj && obj[path] && FileScanner.hasFolderFiles(fromFolder, obj[path])) {
                return compact ? FileScanner.toFilelist(obj[path], fromFolder) : obj[path];
            }
        } else {
            if (!FileScanner.localCache['nodeModulesFiles']) {
                FileScanner.localCache['nodeModulesFiles'] = {};
            }
            const obj = FileScanner.localCache['nodeModulesFiles'];
            if (obj[path] && FileScanner.hasFolderFiles(fromFolder, obj[path])) {
                return compact ? FileScanner.toFilelist(obj[path], fromFolder) : obj[path];
            }
        }
        
        const nodeModulesPackages = (new Packages(null, {
            localPackages: false
        })).exec();
        const nodeModulesFiles = {};
        let nodeModulesFilesArray = [];
        process.chdir(path);
        Object.keys(nodeModulesPackages).map((name) => {
            nodeModulesFiles[name] = {
                files: {},
                order: nodeModulesPackages[name]['order']
            }
            const dir = `${nodeModulesPackages[name]['dir']}/${fromFolder}`;
            if (fs.existsSync(dir)) {
                if (noRead) {
                    nodeModulesFiles[name]['files'][fromFolder] = dir;
                } else {
                    nodeModulesFiles[name]['files'][fromFolder] = 
                        RScandir(dir).map((file) => {
                            const relDir = 
                                nodeModulesPackages[name]['relativeDir'];
                            let dir = 
                                nodeModulesPackages[name]['dir'];
                            file = file.substr(relDir.length);
                            // TODO: This is an extra computation that can be avoided.
                            if (!dir.endsWith('/') && !file.startsWith('/')) {
                                dir += '/';
                            } else if (dir.endsWith('/') && file.startsWith('/')) {
                                file = file.substr(1);
                            }
                            return dir + file;
                        });
                }
            }
        });
        process.chdir(cwd);

        if (FileScanner.Cache.set) {
            let obj = FileScanner.Cache.get('nodeModulesFiles');
            if (!obj) {
                obj = {};
            }
            obj[path] = nodeModulesFiles;
            FileScanner.Cache.set('nodeModulesFiles', obj, {
                persist: true
            });
        } else {
            FileScanner.localCache['nodeModulesFiles'][path] = nodeModulesFiles;
        }
        
        return compact ? FileScanner.toFilelist(nodeModulesFiles, fromFolder) : nodeModulesFiles;
    }

    /**
     * Takes a packages file listing and turns it into a flat filelisting.  
     * Respects the order of the filelisting.
     * @param {Object} filesByPackage 
     */
    static toFilelist(filesByPackage, folder) {
        const fileObjects = [];
        let filelist = [];
        Object.keys(filesByPackage).map((pkg) => {
            const {order, files} = filesByPackage[pkg];
            fileObjects[order] = files[folder];
        });

        for (let files of fileObjects) {
            if (files) {
                filelist = filelist.concat(files);
            }
        }

        return filelist;
    }
}

FileScanner.Cache = {};
FileScanner.localCache = {};
FileScanner.RScandir = RScandir;
module.exports = FileScanner;