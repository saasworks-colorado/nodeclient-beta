/**
 * Helpers container class for Node.js filesystem.  Includes finding first 
 * non-node_modules folder, the original executed file, the current functions
 * parent file, all packages in a directory, resolving packageName/file paths.
 * @class
 */
class FileHelpers {
    /**
     * Gets the most relevant application directory.  This is defined by taking
     * the currently executing file folder, and checking or moving up until the
     * directory is no longer inside a node_modules folder.
     * @param {String} path (Optional) If provided, attempts to lookup from the
     * from directory.
     * @return {String} The most relevant application directory.
     */
    static getApplicationDirectory(path = '') {
        if (!path.length) {
            path = process.cwd();
        }
        path = require('path').resolve(path);
        if (!require('fs').statSync(path).isDirectory()) {
            path = require('path').dirname(path);
        }
        path = FileHelpers.normalize(path);
        while (path.includes('node_modules')) {
            path = path.split('/').slice(0, -1).join('/');
        }
        return FileHelpers.normalize(path);
    }
    
    /**
     * Gets the caller function's filename.  Assumes called function is one 
     * function above Console.getCallerFile function.
     * @author http://stackoverflow.com/questions/16697791/nodejs-get-filename-of-caller-function
     * @return {String} The called function filename.
     */
    static getCallerFile(idx = -1) {
        if (!idx || idx == -1) {
            idx = 0;
        }
        idx += 2;
        if (module['override']) {
            idx += 3; // TODO: This got mucky when combining with a require override.
        }
        return (FileHelpers.getCallerFiles())[idx];
    }

    /**
     * Gets the caller function's filename.  Assumes called function is one 
     * function above Console.getCallerFile function.
     * @author http://stackoverflow.com/questions/16697791/nodejs-get-filename-of-caller-function
     * @return {Array} The callstack.
     */
    static getCallerFiles() {
        let stacktrace = require('stack-trace');
        const files = [];
        stacktrace.get().map((file) => {
            try {
                if (file.isNative() || ((require('path').basename(file.getFileName()) == 'module.js' ||
                    require('path').basename(file.getFileName()) == 'timers.js') &&
                    (require('path').dirname(file.getFileName()) == '.' || 
                    require('path').dirname(file.getFileName()) == 'internal' ||
                    require('path').dirname(file.getFileName()).endsWidth('process')))) {
                } else {
                    files.push(FileHelpers.normalize(file.getFileName()));
                }
            } catch(e) {
                
            }
        });
        return files;
    }

    /**
     * Heuristic for guessing main directory.  This includes resolving the 
     * following use cases: 
     *   1) ./node_modules/.bin scripts
     *   2) ./node_modules/PACKAGE_NAME/file scripts
     *   3) ./SUBDIR/file calls
     * 
     * The first non-node_modules folder with a package.json in it is used.
     */
    static getMainDirectory() {
        return FileHelpers.normalize(FileHelpers.getPackageDirectory(
            FileHelpers.getApplicationDirectory()));
    }

    /**
     * Gets the directory of a package.
     * @param {String} path The path from which to look for the package directory.
     * @returns {String} The directory containing the package.json file.
     */
    static getPackageDirectory(path) {
        return FileHelpers.normalize(FileHelpers.getPackageFile(path).split('/').slice(0, -1).join('/'));
    }
    
    /**
     * Gets the package.json given a file path.  If not found in the filepath 
     * directory, moves up a directory.
     * @param {String} file The file path to search from.
     * @return {Boolean|String} False if no file is found.
     */
    static getPackageFile(path) {
        if (!path) {
            path = process.cwd();
        }

        if (!require('fs').existsSync(path)) {
            return false;
        }

        try {
            path = FileHelpers.normalize(path); // Why would this fail?
        } catch(e) {
            try {
                if (require('fs').existsSync(process.cwd() + '/' + path)) {
                    path = FileHelpers.normalize(process.cwd() + '/' + path);
                }
            } catch(e) {
                return false;
            }
        }

        if (!require('fs').statSync(path).isDirectory()) {
            path = require('path').dirname(path);
        }
        while (!require('fs').existsSync(path + '/package.json') && 
            path.length > 0 && path.split('/')[0].length > 0) {
            path = path.split('/').slice(0, -1).join('/');
        }
        if (require('fs').existsSync(path + '/package.json')) {
            return path + '/package.json';
        } else {
            return '';
        }
    }

    /**
     * Gets the currently executing Node Script's directory.
     * @return {string} The current process target directory.
     */
    static getProcessDirectory() {
        return require('path').dirname(require.main.filename);
    }

    /**
     * Normalize a pathname.
     */
    static normalize(path) {
        // TODO: Enable the mustExist variable.
        return require('path').resolve(path).replace(/\\/g, '/');
    }

    /**
     * A relative path name.
     * @returns {String} A path that is normalized to be relative.  This only
     * works if the provided path is not absolute.
     */
    static relative(path) {
        path = FileHelpers.normalize(path);
        if (path.indexOf(FileHelpers.normalize(process.cwd())) != -1) {
            path = path.substr(process.cwd().length);
            if (path.length == 0) {
                path = '.';
            }
        }

        if (path.substr(0, 1) == '/') {
            path = path.substr(1);
        }

        return path;
    }

    /**
     * Changes a path into a filename.
     * @param {String} path A filepath.
     * @todo Consider removal (used by quality/tests).
     * @returns {String} The filename.
     */
    static toFilename(path) {
        path = path.replace(/\\/g, '/');
        let directories = path.split('/');
        return directories.pop();
    }

    /**
     * Changes an array of paths to an array of filenames.
     * @param {Array} paths An array of paths.
     * @todo Consider removal (used by quality/tests).
     * @returns {Array} An array of filenames.
     */
    static toFilenames(paths) {
        if (typeof paths == 'string') {
            paths = [paths];
        }
        for (let idx in paths) {
            paths[idx] = Files.toFilename(paths[idx]);
        }
        
        return paths;
    }
}

module.exports = FileHelpers;