const events = require('events');
const FileHelpers = require(__dirname + '/filehelpers.class.js');
const Filter = require('ncli-core-helpers/filter.class.js');

/**
 * @enum Describes the detail level for querying packages with.  If no detail,
 * then grabs just the directory name.  Else, grabs package.json contents.
 * All details are all fields that do not begin with an underscore.  Summary
 * includes a curation of most common fields.  Verbose includes the generated
 * fields that begin with the underscore.
 */
const PackageDetailLevel = {
    'NO_DETAIL': 0,
    'ALL_DETAILS': 1,
    'SUMMARY_DETAILS': 2,
    'VERBOSE': 3
};

/**
 * @class 
 * Container class for Node.js filesystem.  Includes finding first 
 * non-node_modules folder, the original executed file, the current functions 
 * parent file, all packages in a directory, resolving packageName/file paths.
 * @todo Ideally, some functionality is moved into a Package class.
 */
class Packages {
    constructor(path = null, options = {}) {
        /**
         * @type {Boolean} If true, recursively search node_modules folders.
         */
        this.deep = false;

        /**
         * @type {Boolean} If true, then shows details with each package 
         * returned such as version number, if it's a symlink 
         * (and src directory if symlink), if package is local.
         */
        this.detailLevel = PackageDetailLevel.ALL_DETAILS;

        /**
         * @type {Boolean} If true, searches node_modules packages in local 
         * packages.
         */
        this.extendedNodeModulesPackages = false;

        /**
         * @type {Boolean} If true, then looks for local packages.
         */
        this.localPackages = true;

        /**
         * @type {Boolean} If true, retrieve packages with the same name for 
         *  each version of the package in node_modules.  Typically works only
         *  in conjunction with Packages.deep.  If false, then only 
         *  retrieves the latest version.
         */
        this.multipleVersions = false;

        /**
         * @type {Boolean} If true, search node_modules packages.
         */
        this.nodeModulesPackages = true;

        /**
         * @type {Array<Object>} The packages found for this path.  Each package
         * is a package object.
         */
        this.packages = [];

        if (!path) {
            path = process.cwd();
        }
        
        path = FileHelpers.normalize(path);

        if (path.substr(1) == '/') {
            path = '.' + path;
        }

        /**
         * @type {String} This is the path string that all folder scans are 
         * based off of.
         */
        this.path = path;

        Object.assign(this, options);
        
        this.getPackages();
    }

    /**
     * Clears out both the local Packages cache AND then calls clear on all
     * caches handled by the packages cache.  Note that this merely clears the
     * in-memory cache.
     * @param {Boolean} fromFilesystem If true, deletes from filesystem as well.
     */
    static clearCache(fromFilesystem = false) {
        Packages.localCache = {};
        if (Packages.Cache.set) {
            Packages.Cache.clear('nodeModulesPackages', fromFilesystem);
            Packages.Cache.clear('localPackages', fromFilesystem);
            Packages.Cache.clear('packageObjects', fromFilesystem);
        }
    }
    
    /**
     * Compares the versions of packages.
     * @param {String} a The first version string, e.g. `0.1.2`
     * @param {String} b The second version string to compare.
     * @returns {Number} 
     */
    static cmpVersions(a, b) {
        if (!a) {
            return -1;
        }
        const v1 = a.split(/\./g).map(num => parseInt(num));
        if (!b) {
            return 1;
        }
        const v2 = b.split(/\./g).map(num => parseInt(num));
        if (v1[0] > v2[0]) {
            return 1;
        } else if (v1[0] < v2[0]) {
            return -1;
        } else if (v1[1] > v2[1]) {
            return 1;
        } else if (v1[1] < v2[1]) {
            return -1;
        } else if (v1[2] > v2[2]) {
            return 1;
        } else if (v1[2] < v2[2]) {
            return -1;
        }
        return 0;
    }

    /**
     * Prepares packages to have their files read.  Achieves this by de-duping 
     * packages by returning first local, then node_modules, then extended
     * node_modules directories.  Finally, if multiple package versions exist
     * within this prioritization, and `multipleVersions` is false, returns 
     * only the highest version.
     * @returns {Array} An array of package names.
     */
    exec() {
        let packages = this.toSortedPackagesObject();
        
        for (let name in packages) {
            // First, cull out directories for duplicate versions of packages.
            for (let version in packages[name]) {
                packages[name][version] = this
                    .toMostRelevantPackage(packages[name][version]);
            }
            
            // Next, if no multiple versions alloewd, remove.
            if (!this.multipleVersions) {
                const highestVersion = Packages
                    .getHighestVersion(Object.keys(packages[name]));
                packages[name] = packages[name][highestVersion];
            }
        }

        for (let name in packages) {
            if (this.multipleVersions) {
                for (let version in packages[name]) {
                    packages[name][version] = this
                        .getPackageDetails(packages[name][version]);
                }
            } else {
                packages[name] = this.getPackageDetails(packages[name]);
            }
        }

        return packages;
    }

    /**
     * Filters the package by name using the Filter object from 
     * `ncli-core-helpers`.
     * @returns {Array} An array of packages matching the query.
     */
    filterPackages(query) {
        const names = this.packages.map(({name}) => name);
        const filteredPackages = Filter.exec(names, query);
        this.packages =
            this.packages.filter(({name}) => filteredPackages.includes(name));
        return this.packages;
    }

    /**
     * @return {Array} An array of package directories.
     */
    getDirectories(local = false, relative = false) {
        return this.packages.filter((pkg) => pkg.isLocal == local)
            .map(({dir, relativeDir}) => relative ? relativeDir : dir);
    }

    /**
     * Gets the extended `node_modules` files, which refers to all local 
     * packages `node_modules` folders.
     * e.g. subdir/packageFolder/node_modules/package
     */
    getExtendedNodeModulesPackages() {
        // For each local package, then run the `getNodeModulesPackages`.
        // Ensure cache is written for both this package AND for that package.
    }

    /**
     * Gets the highest version of the package.
     * @param {Array} versions An array of version strings.
     */
    static getHighestVersion(versions) {
        let highestVersion = null;
        for (let version of versions) {
            if (!version) {
                continue;
            }

            if (!highestVersion) {
                highestVersion = version;
                continue;
            }
            
            if (Packages.cmpVersions(version, highestVersion) >= 0) {
                highestVersion = version;
            }
        }

        return highestVersion;
    }

    /**
     * Gets all the local packages.
     * @returns {Array<Object>} The array of packages filtered for local
     * packages.
     */
    getLocalPackages() {
        return this.packages.filter(({isLocal}) => isLocal);
    }
    
    /**
     * Given some root directory, finds all package.json files outside of 
     * node_modules paths.  Ensure that each package gets its cache written 
     * for its local packages for all files that are within its subdirectory 
     * structure.
     * @param {String} path (Optional) If provided, the root directory from 
     * which to search.  Else, uses process.cwd().
     */
    static getLocalPackages(path = '') {
        if (!path || path.length == 0) {
            path = process.cwd();
        }

        path = FileHelpers.getPackageDirectory(path);
        if (Packages.Cache.set) {
            const obj = Packages.Cache.get('localPackages');
            if (obj && obj[path]) {
                return obj[path];
            }
        } else {
            if (!Packages.localCache['localPackages']) {
                Packages.localCache['localPackages'] = {};
            }
            const obj = Packages.localCache['localPackages'];
            if (obj[path]) {
                return obj[path];
            }
        }

        const files = require('ncli-core-rscandir')(path);

        const packages = files.filter((file) => file.split('/')
            .slice(-1) == 'package.json');
        const localPackages = packages.map((path) => {
            return Packages.getPackageDetailObject(path);
        });

        if (Packages.Cache.set) {
            let obj = Packages.Cache.get('localPackages');
            if (!obj) {
                obj = {};
            }
            obj[path] = localPackages;
            Packages.Cache.set('localPackages', obj, {
                persist: true
            });
        } else {
            Packages.localCache['localPackages'][path] = localPackages;
        }
        return localPackages;
    }

    /**
     * Finds the packages given a directory.
     * @return {Object} An object of package directories, keyed by package name.
     */
    getNames(local = false, relative = false) {
        const obj = {};
        this.packages.filter((pkg) => pkg.isLocal == local)
            .map(({dir, name, relativeDir}) => {
                if (!relative) {
                    obj[dir] = name;
                } else {
                    obj[relativeDir] = name;
                }
            });
        return obj;
    }

    /**
     * Gets all the node_modules packages.
     */
    getNodeModulesPackages() {
        return this.packages.filter(({isLocal}) => !isLocal);
    }
    
    /**
     * Gets all node_modules packages.
     * @param {String} path The path from whence to scan.
     * @param {Boolean} noDepth If true, only scans one package layer deep.
     * @param {Boolean} recursion True only if inside a recursion.
     * @returns {Array} An array of Node packages.
     */
    static getNodeModulesPackages(path = '', noDepth = true, recursion = false) {
        if (!path || path.length == 0) {
            path = process.cwd();
        }
        path = FileHelpers.normalize(path);

        if (Packages.Cache.set) {
            const obj = Packages.Cache.get('nodeModulesPackages');
            if (obj && obj[path]) {
                return obj[path];
            }
        } else {
            if (!Packages.localCache['nodeModulesPackages']) {
                Packages.localCache['nodeModulesPackages'] = {};
            }
            const obj = Packages.localCache['nodeModulesPackages'];
            if (obj && obj[path]) {
                return obj[path];
            }
        }

        let nodeModulesPackages = [];
        const curPath = path + '/node_modules';

        let directories = [];

        // Gets here if there is no cache hit.
        if (!require('fs').existsSync(curPath)) {
            console.warn('No node_modules folders.');
            return nodeModulesPackages;
        }
        
        directories = nodeModulesPackages
            .concat(require('fs').readdirSync(curPath));
        nodeModulesPackages = directories.reduce((packages, dir, idx) => {
            try {
                let pkgDetailObj = Packages
                    .getPackageDetailObject(`${curPath}/${dir}/package.json`);
                if (pkgDetailObj) {
                    packages.push(pkgDetailObj);
                }
                /*
                if (!!progress) {
                    try {
                        process
                            .emit('Packages.getNodeModulesPackages' + 
                            '.readPackage', idx, directories.length, 
                            pkgDetailObj['name'])
                    } catch(e) {}
                }
                */
                if (!noDepth) {
                    packages = packages.concat(Packages
                        .getNodeModulePackages(pkgDetailObj.dir, true, true));
                }
            } catch(e) {
                console.warn(e);
            }
            return packages;
        }, []);

        if (Packages.Cache.set) {
            let obj = Packages.Cache.get('nodeModulesPackages');
            if (!obj) {
                obj = {};
            }
            obj[path] = nodeModulesPackages;
            Packages.Cache.set('nodeModulesPackages', obj, {
                persist: true
            });
        } else {
            Packages.localCache['nodeModulesPackages'][path] = 
                nodeModulesPackages;
        }

        return nodeModulesPackages;
    }

    /**
     * Generates the intermediate package detail object used by the cache.
     * @param {String} path The path of the package.json file.
     * @returns {Object} A normalized details object.
     */
    static getPackageDetailObject(path) {
        if (!require('fs').existsSync(path)) {
            return null;
        }
        const dir = FileHelpers.normalize(path).split(/\//g).slice(0, -1)
            .join('/');
        const isLocal = !dir.includes('/node_modules/');
        const relativeDir = dir.substr(FileHelpers
            .getMainDirectory(path).length + 1);
        const packageJSON = Packages.getPackageObject(path);
        const {name, version} = packageJSON;
        const isSymlink = require('fs').lstatSync(dir).isSymbolicLink();
        const isMain = FileHelpers.getMainDirectory() == dir;
        return {
            dir, 
            isLocal, 
            isMain,
            isSymlink, 
            name, 
            packageJSON, 
            relativeDir, 
            version
        };
    }

    /**
     * Returns the appropriate level of detail given a package object.
     * @param {Object} pkg 
     */
    getPackageDetails(pkg) {
        if (this.detailLevel == PackageDetailLevel.NO_DETAIL) {
            pkg = pkg['dir'];
        } else if (this.detailLevel == PackageDetailLevel.ALL_DETAILS) {
            for (let field in pkg['packageJSON']) {
                if (field.startsWith('_')) {
                    delete pkg['packageJSON'][field];
                }
            }
        }
        return pkg;
    }

    /**
     * Gets the package.json name key given a file path.  If not found in the
     * filepath directory, moves up a directory.
     * @param {String} file The file path to search from.
     * @return {Boolean|String} Name of most relevant package or false if no 
     * file found or malformed JSON.
     */
    static getName(file) {
        const packageFile = FileHelpers.getPackageFile(file);
        if (packageFile) {
            try {
                return Packages.getPackageObject(dir)['name'];
            } catch(e) {
                return false;
            }
        }
        return false;
    }

    /**
     * Gets the package.json object key given a file path.  If not found in the
     * filepath directory, moves up a directory.
     * @param {String} file The file path to search from.
     * @return {Object} Object of most relevant package or false if no 
     * file found or malformed JSON.
     */
    static getPackageObject(file) {
        file = FileHelpers.normalize(file);
        const packageFile = FileHelpers.getPackageFile(file);
        if (!packageFile) {
            return false;
        }
        
        if (Packages.Cache.set) {
            let obj = Packages.Cache.get('packageObjects');
            if (!obj) {
                obj = {};
            }
            if (obj && obj[packageFile]) {
                return obj[packageFile];
            }
        } else {
            if (!Packages.localCache['packageObjects']) {
                Packages.localCache['packageObjects'] = {};
            }
            const obj = Packages.localCache['packageObjects'];
            if (obj && obj[packageFile]) {
                return obj[packageFile];
            }
        }

        try {
            const packageObj = JSON.parse(require('fs').readFileSync(packageFile, 
                'utf8'));
            if (Packages.Cache.set) {
                let obj = Packages.Cache.get('packageObjects');
                if (!obj) {
                    obj = {};
                }
                obj[packageFile] = packageObj;
                Packages.Cache.set('packageObjects', obj, {
                    persist: true
                });
            } else {
                if (!Packages.localCache['packageObjects']) {
                    Packages.localCache['packageObjects'] = {};
                }
                Packages.localCache['packageObjects'][packageFile] = 
                    packageObj;
            }
            return packageObj;
        } catch(e) {
            return false;
        }
    }

    /**
     * Gets all the packages directories given a path input.  This includes
     * direct subfolders of node_modules (if it exists) and all folders with
     * package.json in it.
     * 
     * @param {String} path If provided, concatenates to process.cwd().
     * @returns {Array} An array of package names.
     */
    getPackages() {
        if (!this.path.includes('node_modules') && this.localPackages) {
            this.packages = this.packages
                .concat(Packages.getLocalPackages());
        }
        
        if (this.nodeModulesPackages) {
            this.packages = this.packages.concat(Packages
                .getNodeModulesPackages(this.path, !this.deep)).reverse();
        }

        if (this.extendedNodeModulesPackages) {
            this.packages = this.packages
                .concat(this.getExtendedNodeModulesPackages()).reverse();
        }

        this.packages = this.packages.reverse();
        this.packages.map((pkg, idx) => pkg.order = idx);
        return this.packages;
    }

    /**
     * @param {Array} packages An array of Package objects that specify the 
     * order and directory.
     * @return {Object} The shortest path object, such that the least nested 
     * depth object is returned.  The directory and the order in which 
     * the directory appears is returned.
     */
    static getShortestPath(packages) {
        let shortestPath = null;
        let shortestObj = null;
        for (let pkg of packages) {
            const num = pkg.dir.split(/\//g).length;
            if (!shortestPath || num < shortestPath) {
                shortestPath = num;
                shortestObj = pkg;
            }
        }

        return shortestObj;
    }

    /**
     * Given a list of paths of packages, returns the packages listed by name 
     * and then by version.
     * @returns {Object} A sorted list of packages, keyed by name, then version.
     */
    toSortedPackagesObject() {
        const packages = {};
        this.packages.map((pkg) => {
            if (!packages[pkg.name]) {
                packages[pkg.name] = {};
            }
            if (!packages[pkg.name][pkg.version]) {
                packages[pkg.name][pkg.version] = [];
            }
            packages[pkg.name][pkg.version].push(pkg);
        });
        return packages;
    }

    /**
     * Get the best package for the name/version combination.  This is based
     * first on local package/node_modules/extended node_modules, then next
     * on the shortest path.
     * @param {Array} packages An array of package objects.
     * @returns {Object} A Package object 
     */
    toMostRelevantPackage(packages) {
        // If has local package, cull out non-local packages.
        if (packages.find(({isLocal}) => isLocal)) {
            packages = packages.filter(({isLocal}) => isLocal);
        }

        // Right meow, we have shortest path.
        return Packages.getShortestPath(packages);
    }
}

// This local and simple cache speeds up performance before the cache loads.
Packages.Cache = {};
Packages.localCache = {};
Packages.DetailLevel = PackageDetailLevel;
module.exports = Packages;