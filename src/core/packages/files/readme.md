# Files

Recursive filename retrieval using the `node_modules` filesystem.  Includes 
FileWriter and utility functions.  Files are delivered first from 
`node_modules`, then from subfolder packages, then from the executing package.

## Example Usage

Use `require('ncli-core-files').readFiles(fromFolder);` to retrieve an array 
listing of files in the `fromFolder` subfolder.  Reads occur in the executing 
package, subpackages of the executing package and the `node_modules` diretory.

## `require('ncli-core-files')` behavior
Exports a Files class instantiation.

## Caching

There are a total of four caches that exist for this package.  Each cache has 
different behavior depending on the environment variable set
(e.g. either env=dev or env=prod).  

### packageObjects

Cache for all package objects reads.  This parses `package.json` files 
into JSON objects.  Cache is keyed by the path of the package.

#### Format
<pre>
{packagePath: {
    dir: String,
    lastModified: Number,
    packageJSON: Object,
    version: String
}, packagePath2: Object}
</pre>

#### Dev/Prod Differences

`prod` reads `package.json` objects once, `dev` reads `package.json`
objects once per run ONLY IF a package is in the local filesystem AND
a stat of the file shows a more recent `lastModified` time.

### nodeModulesPackages

Cache for all packages stored in `node_modules`.  Keyed by the path 
where lookup is performed, usually one directory up from `node_modules`.
Next, cache is keyed by the package name, and finally with package 
details.  The pacakge name is the key only because it is assumed that 
all paths follow the format `${lookupPath}/node_modules/${packageName}`.

#### Format
<pre>
{lookupPath: {
        packageName1: {
            dir: String,
            isSymlink: Boolean,
            name: String,
            packageJSON: Object,
            version: String
        }
    }
}
</pre>

#### Dev/Prod Differences

Both `prod` and `dev` reads this once on install and update, and
forever reads from the cache.  

### localPackages

Cache for all packages stored in local filesystem.  Keyed by the path 
where the local scan originated, then by the path of the local package.

Uniquely, when `localPackages` cache is saved it is also saved into the package
directories containing the discovered packages.  The reason is that it is 
helpful to save on the number of local filesytem reads.

#### Format
<pre>
{lookupPath: {
        localPackagePath1: {
            dir: String,
            isSymlink: Boolean,
            name: String,
            packageJSON: Object,
            version: String
        }
    }
}
</pre>

#### Dev/Prod Differences

`prod` reads once on install and update.  `dev` reads this once on 
startup.  `dev` can disable a local scan with `--no-local-scan`.  
Alternately, local scans can be avoided under `dev` by setting the 
`ncli` config property `noLocalDevScan` to true.

### fileFolders

Cache for all file folders requested.  This cache consists of all file folders
populated by the configuration variables specified and by folders requested at 
runtime.  This is primarily helpful by reading folders specified at runtime.  
When this cache is flushed, it is rebuilt using the configuration folders 
specified by the `ncli` config.

<pre>
[{
    name: String,
    source: String
}]
</pre>

#### Dev/Prod Differences

The behavior is the same for development and for production.

### nodeModulesFiles

Cache for all files found within folders of packages within 
`node_modules`.  This cache is built upon a a files read or can be 
configured to read on startup (preferred method).<br>
If a file read is initiated before a `nodeModulesPackages` cache exists,
then the `nodeModulesPackages` cache is generated.<br>
Cache is stored by lookup path, usually the directory above 
`node_modules`.  Next, cache is stored by packageName, and finally the 
files folder with the files discovered.  For simplicity, file paths 
are stored as absolute paths.<br>
The order of the package is stored as well to preserve the order in which 
the package was discovered.  

#### Format

<pre>
{lookupPath: {
        packageName1: {
            files: {
                fileFolder: [
                    file1,
                    file2,
                    file3
                ],
            },
            order: Number
        }
    }
}
</pre>

#### Dev/Prod Differences

Both `prod` and `dev` reads once on install and update using the `fileFolders`
cache and the configured folders.  When a new folder is requested to be read,
then the `fileFolders` cache shall update and the `nodeModulesFiles` cache
shall be updated.

### localFiles

Cache for all files found within folders of local packages.  This cache is built
upon a a files read or can be configured to read on startup 
(preferred method).<br> If a file read is initiated before a `localPackages`
cache exists, then the `localPackages` cache is generated.<br> Cache is stored
in each package folder underneath the originating search path.  Next, cache is 
stored by packageName, and finally the files folder with the files discovered.  For simplicity, file paths are stored as absolute paths.<br>
The order of the package is stored as well to preserve the order in which 
the package was discovered.  

#### Format

<pre>
{lookupPath: {
        packageName1: {
            files: {
                fileFolder: [
                    file1,
                    file2,
                    file3
                ]
            },
            order: Number
        }
    }
}
</pre>

#### Dev/Prod Differences

Under `prod`, `localFiles` are read once on install and update using the
`fileFolders` cache and the configured folders.  When a new folder is requested
to be read, then the `fileFolders` cache shall update and the `localFiles` cache
shall be updated.

Under `dev`, `localFiles` are read on startup.  If `--no-local-scan` is set or
if the `ncli` config property `noLocalDevScan` to true, then only the existing 
cached `localPackages` are read for files on startup.

## Dependencies
<table>
    <tr>
        <td>Name</td>
        <td>Description</td>
    </tr>
    <tr>
        <td><strong>chalk</strong></td>
        <td>Creates pretty error messages.</td>
    </tr>
    <tr>
        <td><strong>fs-extra</strong></td>
        <td>Moves files.</td>
    </tr>
    <tr>
        <td><strong>rimraf</strong></td>
        <td>Removes directories and subdirectories in one call.</td>
    </tr>
    <tr>
        <td><strong>stack-trace</strong></td>
        <td>Gets filename of currently executing function's caller.</td>
    </tr>
</table>

## File Contexts

There are five file contexts.

<table>
    <tr>
        <td>Name</td>
        <td>Description</td>
        <td>Example</td>
    </tr>
    <tr>
        <td>Local</td>
        <td>The filesystem relative to which file was called when executing 
            Node.</td>
        <td>
        * `node index.js`
        </td>
    </tr>
    <tr>
        <td>Script</td>
        <td>The filesystem relevant to the first non-`node_modules` folder with
            a `package.json` the executing script.</td>
        <td>
        * `node_modules/bin/foobar`
        </td>
    </tr>
    <tr>
        <td>Global Script</td>
        <td>The filesystem relevant to the first non-`node_modules` folder with
            a `package.json` relative to the current working directory 
            associated with the Node process.</td>
        <td>
        * /usr/local/lib/foobar
        * %AppData%/npm/foobar
        </td>
    </tr>
    <tr>
        <td>node_modules Package</td>
        <td>The filesystem relevant to a package relative to the currently
            executing file.  Looked up by package name as defined by folder
            name directly in `node_modules` folder.</td>
        <td>
        * `node_modules/foo/bar.js`
        </td>
    </tr>
    <tr>
        <td>Local Package</td>
        <td>The filesystem relevant to a package relative to the currently
            executing file.  Looked up by package name as defined by name field
            in `package.json` file.  Requires a local file scan of 
            packages.</td>
        <td>
        * `foo/bar.txt`
        * `foo/package.json`
        </td>
    </tr>
    <tr>
        <td>node_modules Package</td>
        <td>The filesystem relevant to a package relative to the currently
            executing file.  Looked up by package name as defined by folder
            name directly in `node_modules` folder.</td>
        <td>
        * `node_modules/foo/bar.js`
        </td>
    </tr>
</table>

## Packages Class

Package helpers and container class.  Solves common use cases for interacting 
with a Node.js development filesystem.  Brings various dependencies into one 
place.

* Change directory to originally executed filepath.
* Get the most relevant `package.json` __name__ field given a __file__.
* Get first folder not contained in a `node_modules` path if executing from a
 `node_modules` context.
* Given some filepath, find all package.json files not contained in a
  `node_modules` file tree.
* Resolve absolute file paths given a __PackgeName__/__File__ format when local
 use and package use is desired.
 
## FileScanner Class

Provides file reading helperes for local and package contexts.  Additionally 
provides interface for whitelisting and blacklisting packages.

## FileWriter Class

Provides file writing helpers for writing files to local and package contexts.  
Writes files irrespective of file path existing (i.e. creates folders).  
Manages temporary files (i.e. removes on process exit).  Moves files in bulk.

<br>
<img src="../../media/logo.png" width="250"/>