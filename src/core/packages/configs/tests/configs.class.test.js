'use strict';

describe('Configs Class', function() {
    let configs, cwd, fs, helpers;
    beforeEach(() => {
        cwd = process.cwd();
        process.chdir(__dirname + '/../');
        fs = require('fs');
        if (!fs.existsSync('configs')) {
            fs.mkdirSync('configs');
        }
        configs = require(process.cwd() + '/configs.class.js');
        try {
            helpers.cleanupConfigs();
        } catch(e) {}
        try {
            configs.cleanup();
            process.emit('Configs Cleanup');
        } catch(e){}
        helpers = require(__dirname + '/test.helpers.js');
        helpers.createConfigs();
        process.chdir(__dirname + '/../');
    });

    it('Should scan configs in the process directory.', () => {
        let configsObj;
        configsObj = configs.readJSON();
        expect(configsObj.hasOwnProperty('test1')).toBe(true);
        expect(configsObj['a']['a']).toBe('b');
        expect(configsObj['a']['toplevelNestedTest']).toBe('foobar');
        expect(configsObj['a']['toplevelTest']).toBe('foobar2');
        expect(configsObj['test1']['a']).toBe('b');
    });

    it('Should provide consistent results for local scans, node_modules scans.', () => {

    });

    it('Should scan using a custom directory,', function() {
        let configsObj;
        configsObj = configs.readJSON('configs');
        expect(configsObj.hasOwnProperty('test1')).toBe(true);
        configsObj = configs.readJSON('customConfigs');
        expect(configsObj.hasOwnProperty('test2')).toBe(true);
        expect(configsObj['test2']['c']).toBe('d');
    });

    it('Should scan all modules and properly allow overrides.', function() {
        let configsObj;
        configsObj = configs.readJSON('configs');
        expect(configsObj.hasOwnProperty('test1')).toBe(true);
        expect(configsObj['test1']['a']).toBe('b');
        expect(configsObj['test1']['b']).toBe('c');
    });

    it('Should update the cache on each load.', function() {
        let settings;
        expect(require.cache).not.toBe(null);
        settings = require(process.cwd() + '/index.js');
        expect(settings['test1']['a']).toBe('b');
        fs.writeFileSync('configs/test1.json', '{"a": "a"}');
        settings = require(process.cwd() + '/index.js');
        expect(settings['test1']['a']).toBe('a');
    });

    it('Should allow defaults to be supplied for programmatic control.', 
        function() {
        let configsObj, settings;
        configsObj = require(process.cwd() + '/configs.class.js');
        configsObj.setDefault('test1', {'c': 'd'});
        settings = configsObj.all();
        expect(settings['test1']['c']).toBe('d');
    });

    it('Should persist defaults across the config object.', function() {
        let configsObj, settings;
        configsObj = require(process.cwd() + '/configs.class.js');
        configsObj.setDefault('test1', {'c': 'd'});
        settings = configsObj.all();
        expect(settings['test1']['c']).toBe('d');
        settings = require(process.cwd() + '/index.js');
        expect(settings['test1']['c']).toBe('d');
    });

    it('Should allow overrides to be supplied for programmatic control.', 
        function() {
        let configsObj, settings;
        configsObj = require(process.cwd() + '/configs.class.js');
        configsObj.setOverride('test1', {'a': 'c'});
        settings = configsObj.all();
        expect(settings['test1']['a']).toBe('c');
    });


    it('Should persist oveerrides across the config object.', function() {
        let configsObj, settings;
        configsObj = require(process.cwd() + '/configs.class.js');
        configsObj.setOverride('test1', {'a': 'c'});
        settings = configsObj.all();
        expect(settings['test1']['a']).toBe('c');
        settings = require(process.cwd() + '/index.js');
        expect(settings['test1']['a']).toBe('c');
    });

    it('Should not scan a non-json file.', function() {
        let settings;
        fs.writeFileSync('configs/test3.jsonfile', '{"a": "z"}');
        fs.writeFileSync('configs/test3.txt', '{"a": "1", "b": "2"}');
        settings = require(process.cwd() + '/index.js');
        expect(settings.hasOwnProperty('test3')).toBe(false);
        fs.unlinkSync('configs/test3.jsonfile');
        fs.unlinkSync('configs/test3.txt');
    });

    // TODO: Reimplement with watchers class.
    xit('Should automatically reload files when config is set.', function(done) {
        let completed, configsObj, settings;
        completed = false;
        configsObj = require(process.cwd() + '/configs.class.js');
        helpers.createConfig('configs/test1.json', JSON.stringify({
            'a': 'a'
        }));
        settings = configsObj.readJSON('configs', (newConfigs) => {
            if (!completed && Object.keys(newConfigs).length > 0) {
                completed = true;
                expect(newConfigs['test1']['a']).toBe('z');
                done();
            }
        }, 10);
        expect(settings['test1']['a']).toBe('a');
        helpers.createConfig('configs/test1.json', JSON.stringify({
            'a': 'z'
        }));
    });

    afterEach(function() {
        helpers.cleanupConfigs();
        configs.cleanup();
        process.emit('Configs Cleanup');
        configs.files.rrmdir(__dirname + '/../.cache');
        configs.files.clearCache();
        configs.packages.clearCache();
        process.chdir(cwd);
    });
});