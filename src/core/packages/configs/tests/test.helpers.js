'use strict';

class ConfigsTestHelpers {
    constructor() {
        this.chalk = require('chalk');
        this.files = require('ncli-core-files').Files;
        this.fs = require('fs');
        this.rimraf = require('rimraf');
    }


    /**
     * Creates config file for testing.
     */
    createConfig(file, contents) {
        this.files.writeFile(file, contents);
    }

    /**
     * Creates config files for testing.
     */
    createConfigs() {
        this.files.addFiles({
            'configs/a.json': '{"a": "b"}',
            'configs/test1.json': '{"a": "b"}',
            'configs/subdir/test1.json': '{}',
            'customConfigs/test2.json': '{"c": "d"}',
            'localPackage/package.json': '{"name": "foobar", "version": "0.0.1"}',
            'localPackage/configs/a.json': '{"a": "c", "b": "foobar", "toplevelNestedTest": "foobar", "toplevelTest": "foobar"}',
            'localPackage/subdir/package.json': '{"name": "foobar-sub", "version": "0.0.1"}',
            'localPackage/subdir/configs/a.json': '{"a": "e", "b": "foobar-sub", "toplevelNestedTest": "foobar-sub", "toplevelTest": "foobar-sub"}',
            'localPackage2/package.json': '{"name": "foobar2", "version": "0.0.1"}',
            'localPackage2/configs/a.json': '{"a": "c", "b": "foobar2", "toplevelTest": "foobar2"}',
            'node_modules/ConfigsTestModule/package.json': '{"name": "foo"}',
            'node_modules/ConfigsTestModule/configs/a.json': '{"a": "123", "b": "c"}',
            'node_modules/ConfigsTestModule/configs/test1.json': '{"a": "123", "b": "c"}',
            'node_modules/ConfigsTestModule/configs/subdir/test1.json': '{}',
            'node_modules/ConfigsTestModule/customConfigs/test2.json': '{}',
            'node_modules/ConfigsTestModule/node_modules/configsNestedTestModule/package.json': '{"name": "bar"}',
            'node_modules/ConfigsTestModule/node_modules/configsNestedTestModule/configs/test1.json': '{"b": "d", "c": "d"}',
            'node_modules/ConfigsTestModule/node_modules/configsNestedTestModule/configs/subdir/test1.json': '{}',
            'node_modules/ConfigsTestModule/node_modules/configsNestedTestModule/customConfigs/test1.json': '{}'
        });
    }

    /**
     * Cleans up the configs created for the testing.
     */
    cleanupConfigs() {
        this.fs.unlinkSync('configs/a.json');
        this.fs.unlinkSync('configs/test1.json');
        this.fs.unlinkSync('configs/subdir/test1.json');
        this.fs.rmdirSync('configs/subdir');
        this.rimraf.sync('customConfigs');
        this.rimraf.sync('localPackage');
        this.rimraf.sync('localPackage2');
        this.rimraf.sync('node_modules/ConfigsTestModule');
    }
}

module.exports = new ConfigsTestHelpers();