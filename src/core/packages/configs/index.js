const files = require('ncli-core-files').Files;
const Configs = require(__dirname + '/configs.class.js');
delete require.cache[module.filename];
if (module['override']) {
    delete module['override'].Require.cache[module.filename];
}
try {
    const mainDirectory = files.getMainDirectory();
    const currentPackageDirectory = files.getPackageDirectory(files
        .getCallerFile(1));
    if (currentPackageDirectory != process.cwd()) {
        // Usually inside a lifecycle script, these are different.
        const cwd = process.cwd();
        process.chdir(currentPackageDirectory);
        // TODO: Is it possible to flush cache here?
        let configs = Configs.readLocalJSON('configs');
        process.chdir(mainDirectory);
        Configs.deepAssign(configs, Configs.readLocalJSON());
        process.chdir(cwd);
        module.exports = configs;
    } else {
        module.exports = Configs.readLocalJSON();
    }
} catch(e) {
    console.log(e);
    module.exports = Configs.readLocalJSON();
}