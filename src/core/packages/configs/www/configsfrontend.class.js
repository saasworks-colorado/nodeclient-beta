class Configs {
    constructor() {
        this.getStatus();
    }

    /**
     * Creates an editor for a config file.
     */
    createConfigEditor(filename, config) {
        let el, schema;
        schema = {};
        el = $('.file.hidden').clone();
        $(el).removeClass('hidden');
        $(el).find('.filename').text(filename);
        for (let key in config) {
            schema[key] = {
            };
            schema[key]['default'] = config[key];
        }
        $('.file.hidden').after(el);
        JSONEditor.defaults.options.collapsed = true;
        JSONEditor.defaults.options.disable_array_add = true;
        JSONEditor.defaults.options.disable_array_delete = true;
        JSONEditor.defaults.options.disable_array_reorder = true;
        JSONEditor.defaults.options.disable_collapse = true;
        JSONEditor.defaults.options.disable_edit_json = true;
        JSONEditor.defaults.options.disable_properties = true;
        JSONEditor.defaults.options.theme = 'bootstrap3';
        new JSONEditor($(el).find('.json-editor').get(0), {
            schema: {
                title: filename,
                type: 'object',
                options: {
                    collapsed: true,
                    disable_collapse: false
                },
                properties: schema
            }
        });
    }

    /**
     * Gets the current status of modules.
     */
    getStatus() {
        $.ajax({
            url: '/configs/get',
            type: 'get',
            success: (obj) => {
                for (let file in obj) {
                    this.createConfigEditor(file, obj[file]);
                }
            }
        });
    }
}