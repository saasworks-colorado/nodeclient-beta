"use strict";

class RScandirTestHelpers {
    constructor() {
        this.fs = require('fs');
        this.rimraf = require('rimraf');
    }

    /**
     * Creates files for use in the tests.
     */
    createFiles() {
        let cwd;
        cwd = process.cwd();
        process.chdir(__dirname + '/../');

        if (!this.fs.existsSync('rscandirFiles')) {
            this.fs.mkdirSync('rscandirFiles');
        }

        this.fs.writeFileSync('rscandirFiles/a.json', '{"a": "b"}');
        this.fs.writeFileSync('rscandirFiles/z.json', '{"b": "c"}');
        process.chdir('rscandirFiles');
        if (!this.fs.existsSync('subdir')){
            this.fs.mkdirSync('subdir');
        }
        this.fs.writeFileSync('subdir/a.json',
            '{"a": "b"}');
            
        process.chdir(__dirname + '/../');

        if (!this.fs.existsSync('node_modules')){
            this.fs.mkdirSync('node_modules');
        }
        process.chdir('node_modules');
        if (!this.fs.existsSync('rscandirFilesTestModule')){
            this.fs.mkdirSync('rscandirFilesTestModule');
        }
        process.chdir('rscandirFilesTestModule');
        this.fs.mkdirSync('node_modules');
        this.fs.mkdirSync('rscandirFiles');
        this.fs.writeFileSync('rscandirFiles/a.json',
            '{"a": "123", "b": "c"}');
        this.fs.writeFileSync('rscandirFiles/z.json',
            '{"a": "234", "b": "d"}');
        process.chdir('node_modules');
        this.fs.mkdirSync('nestedFolder');
        process.chdir('nestedFolder');
        this.fs.writeFileSync('a.json',
            '{"b": "d", "c": "d"}');
        this.fs.mkdirSync('rscandirFiles');
        this.fs.writeFileSync('rscandirFiles/a.json',
            '{"b": "d", "c": "d"}');
        this.fs.writeFileSync('rscandirFiles/z.json',
            '{"b": "123", "c": "234"}');
        this.fs.mkdirSync('node_modules');
        process.chdir('node_modules');
        this.fs.mkdirSync('nestedFolder');
        process.chdir('nestedFolder');
        this.fs.mkdirSync('rscandirFiles');
        this.fs.writeFileSync('rscandirFiles/a.json',
            '{"b": "e", "c": "f"}');
        process.chdir(__dirname + '/../');
        process.chdir(cwd);
    }

    cleanupFiles() {
        let cwd;
        cwd = process.cwd();
        process.chdir(__dirname + '/../');
        this.rimraf.sync('rscandirFiles');
        this.rimraf.sync('node_modules/rscandirFilesTestModule');
        process.chdir(cwd);
    }
}
module.exports = new RScandirTestHelpers();