describe('RScandir', function() {
    let cwd, fs, helpers, rimraf, rscandir, rscandirClass,  rscandirHelpers;

    beforeEach(function() {
        cwd = process.cwd();
        process.chdir(__dirname + '/../');
        fs = require('fs');
        helpers = require(__dirname + '/test.helpers.js');
        rimraf = require('rimraf');
        // rimraf.sync(__dirname + '/../cache');
        rscandir = require(process.cwd() + '/index.js');
        rscandirClass = require(process.cwd() + '/rscandir.class.js');
        rscandirHelpers = require(process.cwd() + '/rscandir.class.js')
            .instance;
        helpers.cleanupFiles();
        helpers.createFiles();
        process.chdir(__dirname + '/../');
    });

    describe('Basic Usage.', () => {
        it('Should scan all rscandirFiles in a directory.', () => {
            rscandirHelpers.setIgnore([rscandirClass
                .RScandirIgnoreStrategy.NONE]);
            expect(rscandir('rscandirFiles').length).toBe(3);
            for (let file of rscandir('rscandirFiles')) {
                expect(fs.existsSync(file)).toBe(true);
            }
        });
    
        it('Should scan all rscandirFiles of a subdirectory.', () => {
            rscandirHelpers.setIgnore([rscandirClass
                .RScandirIgnoreStrategy.NONE]);
            expect(rscandir('node_modules/rscandirFilesTestModule').length)
                .toBe(6);
            for (let file of
                rscandir('node_modules/rscandirFilesTestModule')) {
                expect(fs.existsSync(file)).toBe(true);
            }
        });

        it('Should allow just directories to be scanned.', () => {
            rscandirHelpers.setIgnore([rscandirClass
                .RScandirIgnoreStrategy.NONE]);
            let directories;
            directories = rscandirHelpers
                .walkDirectoriesSync('node_modules/rscandirFilesTestModule');
            directories.map((dir) => {
                expect(require('fs').statSync(dir).isDirectory()).toBe(true);
            });
            expect(directories.length).toBe(7);
        });
    });

    describe('Sort Strategies.', () => {
        it('Should scan all rscandirFiles of a subdirectory using a custom' +
        ' sort strategy.', () => {
            rscandirHelpers.setIgnore([rscandirClass
                .RScandirIgnoreStrategy.NONE]);
            let sorts = require(process.cwd() + '/rscandir.class.js')
                .RScandirSortStrategy;
            rscandirHelpers.setSort(sorts.DEPTH_FIRST);
            expect(rscandir('node_modules/rscandirFilesTestModule').length)
                .toBe(6);
            let lastDepth = -1;
            for (let file of
                rscandir('node_modules/rscandirFilesTestModule')) {
                if (lastDepth >= 0) {
                    expect(lastDepth).toBeGreaterThanOrEqual(lastDepth);
                }
                lastDepth = file.split('/').length;
                expect(fs.existsSync(file)).toBe(true);
            }
        });
    });

    describe('Filtering.', () => {
        it('Should allow results to be filtered.', () => {
            rscandirHelpers.setIgnore([rscandirClass
                .RScandirIgnoreStrategy.NONE]);
            let files;
            files = rscandir('node_modules/' +
                    'rscandirFilesTestModule', '*/a*.json');
            expect(files.length).toBe(4);
            files = rscandir('node_modules/' +
                    'rscandirFilesTestModule', 'a.json');
            expect(files.length).toBe(4);
        });
        
        it('Should allow just directories to be scanned and filtered.', () => {
            rscandirHelpers.setIgnore([rscandirClass
                .RScandirIgnoreStrategy.NONE]);
            let directories;
            directories = rscandirHelpers
                .walkDirectoriesSync('node_modules/' +
                    'rscandirFilesTestModule', '*nestedFolder*');
            expect(directories.length).toBe(5);
            
            directories = rscandirHelpers
                .walkDirectoriesSync('node_modules/' +
                    'rscandirFilesTestModule', 'node_modules');
            expect(directories.length).toBe(2);
        });

        describe('Advanced filtering.', () => {
            it('Should do minimatch filtering.', () => {
                expect(rscandir('node_modules/' +
                    'rscandirFilesTestModule', {
                        pattern: '*/*/*/*/a.json',
                        type: 'glob'
                    }).length).toBe(1);
            });

            it('Should do wildstring filtering.', () => {
                expect(rscandir('node_modules/' +
                    'rscandirFilesTestModule', {
                        pattern: '*/*/*/**/a.json',
                        type: 'wildstring'
                    }).length).toBe(3);
            });
            
            it('Should do regex filtering.', () => {
                expect(rscandir('node_modules/' +
                    'rscandirFilesTestModule', {
                        pattern: '.+a\.json',
                        type: 'regex'
                    }).length).toBe(4);
            });

            it('Should elegantly handle filtering shorthand.', () => {
                // Wildcard strategy.
                expect(rscandir('node_modules/rscandirFilesTestModule/*').length)
                    .toBe(6);
    
                // Glob strategy.
                expect(rscandir('node_modules/rscandirFilesTestModule/*/*/**/*.json').length)
                    .toBe(4);
    
                // Wildcard strategy.
                expect(rscandir('node_modules/rscandirFilesTestModule/*/*/**/*.json', 'wildstring').length)
                    .toBe(3);
            });
        });
    });
    
    describe('Tests ignoring node_modules.', () => {

    });
    
    describe('Tests ignoring dot folders.', () => {

    });
    
    describe('Tests ignoring .gitignore files and folders.', () => {

    });

    afterEach(() => {
        helpers.cleanupFiles();
        process.chdir(cwd);
    });
});