describe('Console', function() {
    let consoleLog, Console;
    consoleLog = console.log;
    Console = require('../console.class.js').class;
    beforeEach(() => {
        consoleObj = require('../console.class.js');
    });
    it('Should prevent console from logging messages threshold is exceeded.', 
        () => {
            spyOn(Console, '_log').and.callThrough();
            consoleObj.configs['console']['blacklist'] = [];
            consoleObj.configs['console']['whitelist'] = [];
            consoleObj.thresholdCount = 2;
            console.log('test1');
            console.log('test2');
            console.log('test3');
            expect(Console._log).toHaveBeenCalledTimes(2);
            consoleObj.resetStates();
            Console._log.calls.reset();
            consoleObj.thresholdCount = 1;
            console.log('test1');
            console.log('test2');
            console.log('test3');
            expect(Console._log).toHaveBeenCalledTimes(1);
        });

    it('Should log messages to filesystem if threshold is exceeded.', () => {
    });

    afterEach(() => {
        Console.restoreConsole();
        delete Console.instance;
    });
});