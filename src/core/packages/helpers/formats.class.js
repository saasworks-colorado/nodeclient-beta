/**
 * Helper class for file format conversion.  Designed to work both client and 
 * server-side.  
 * @class
 */
class Formats {
    /**
     * Converts to a JSON object from a CSV string or file.
     * @param {String} csv A CSV string or file.  
     * @param {String} filename If provided, saves output to this file.
     */
    static toJSONfromCSV(csv, filename = null) {
        const lines = csv.split(/\n/g);
        const json = [];
        
        const getLine = (row) => {
            let idx = 0;
            let startIdx = 0;
            let keyIdx = 0;
            const values = [];
            while (idx < row.length) {
                let char = row[idx];
                if (char == '"') {
                    do {
                        char = row[++idx];
                    } while (char !== '"' && idx < row.length - 1);
                }
                if (char === ',' || idx === row.length - 1) {
                    if (idx === row.length - 1) {
                        idx++;
                    }
                    let value = row.substr(startIdx, idx - startIdx).trim();
                    if (value[0] === '"') {
                        value = value.substr(1);
                    }
                    if (value[value.length - 1] === ',') {
                        value = value.substr(0, value.length - 1);
                    }
                    if (value[value.length - 1] === '"') {
                        value = value.substr(0, value.length - 1);
                    }
                    values.push(value.replace(/""/g, '"'));
                    startIdx = idx + 1;
                }
                ++idx;
            }
            return values;
        };

        const headers = getLine(lines.shift());
        return lines.map((line) => {
            const rowObj = {};
            getLine(line).map((value, idx) => {
                return rowObj[headers[idx]] = value.replace(/\\n/g, '\n');
            });
            return rowObj;
        });
    }

    /**
     * Converts to a CSV object from a JSON string or file.
     * @param {String} json A JSON string or file.  
     * @param {String} filename If provided, saves output to this file.
     */
    static toCSVfromJSON(json, filename = null) {
        if (!json) {
            return '';
        }
        const replacer = (key, value) => {
            if (typeof value == 'number') {
                value = value.toString();
            }
            value = !value ? '' : value.replace(/"/g, '""');
            return value;
        }
        // Ensure all keys are captured.
        json.map((item) => {
            for (let key in item) {
                if (!json[0][key]) {
                    json[0][key] = '';
                }
            }
        });
        const header = Object.keys(json[0]);
        let csv = json.map(row => header
            .map(fieldName => JSON.stringify(row[fieldName], replacer))
            .join(','));
        csv.unshift(header.join(','))
        csv = csv.join('\n').replace(/\\\"/g, '"')
        return csv;
    }
}

if (typeof module != 'undefined') {
    module.exports = Formats;
}