module.exports = {
    Filter: require(__dirname + '/filter.class.js'),
    Formats: require(__dirname + '/formats.class.js'),
    Override: require(__dirname + '/override.class.js'),
    Require: require(__dirname + '/require.class.js'),
    Shutdown: require(__dirname + '/shutdown.class.js')
}