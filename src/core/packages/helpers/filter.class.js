/**
 * Simple filter utilizes a wildcard match, regex match, or function style 
 * filtering.
 */
class Filter {
    /**
     * @param {Array} arr The list of strings to filter.
     * @param {String|RegExp|{pattern: String|RegExp, exclude: Boolean, type: String}} filter 
     * If is a string, is a filter for wildstring/minimatch.  If is a RegExp, 
     * performs a match.  Exclude option may be set as an object.
     */
   constructor(arr, filter) {
       /**
        * @type {Array} The input list of strings.
        */
       this.arr = arr;

       /**
        * @type {Boolean} If true, the filter acts as an exclusion filter
        * instead of an inclusion filter. 
        */
       this.exclude = false;

       /**
        * @type {Array} The filtered list of strings.
        */
       this.filtered = [];

       /**
        * @type {String} The pattern to match.
        */
       this.pattern = '*';

       /**
        * @type {String} The type of match to perform.
        */
       this.type = 'wildstring';
       
       if (typeof filter == 'object' && !filter.exec) {
           Object.assign(this, filter);
       }
       if (typeof filter != 'object' || !filter.type) {
           if (typeof filter != 'object' || filter.exec) {
               this.pattern = filter;
           }
           if (this.pattern.exec) {
               this.type = 'regex';
           } else if (this.pattern.includes('*')) {
               this.type = 'wildstring';
           } else {
               // TODO: Should this happen?
               this.pattern = '*' + this.pattern;
               this.type = 'wildstring';
           }
       }

       for (let file of this.arr) {
           const match = this.match(file);
           if (match && !this.exclude) {
               this.filtered.push(file);
           } else if (!match && this.exclude) {
               this.filtered.push(file);
           }
       }
    }

    static exec(arr, filter) {
        return (new Filter(arr, filter)).filtered;
    }

    /**
     * Pattern match.
     * @return {Boolean} True if file matches pattern using configured method.
     */
    match(str, filter) {
        if (!str) {
            return false;
        }
        switch (this.type) {
            case 'regex':
                return str.match(this.pattern);
            break;
            case 'wildstring':
                return require('wildstring').match(this.pattern, str);
            break;
        }
    }
}

module.exports = Filter;