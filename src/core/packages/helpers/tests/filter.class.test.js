const Filter = require(__dirname + '/../filter.class.js');
describe('Filter', () => {
    it('Should run a regex filter.', () => {
        const result = 
            Filter.exec(['a', 'b', 'c', 'dad', 'dae', 'da0'], /[0-9]/g);
        expect(result.length).toBe(1);
        expect(result[0]).toBe('da0');
    });

    it('Should run a wildstring filter.', () => {
        const result = 
            Filter.exec(['a', 'b', 'c', 'dad', 'dae', 'da0'], 'd*');
        expect(result.length).toBe(3);
        expect(result[0]).toBe('dad');
        expect(result[1]).toBe('dae');
        expect(result[2]).toBe('da0');
    });

    it('Should test against a plaintext str.', () => {
        const result = 
            Filter.exec(['a', 'b', 'c', 'dad', 'dae', 'da0'], 'dad');
        expect(result.length).toBe(1);
        expect(result[0]).toBe('dad');
    });
});