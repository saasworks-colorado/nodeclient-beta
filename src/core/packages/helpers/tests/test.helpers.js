
class HelpersTestHelpers {
    createFiles() {
        try {require('fs').mkdirSync(__dirname + '/.test');} catch(e) {}
        Object.keys(HelpersTestHelpers.files).map((file) => {
            let contents = HelpersTestHelpers.files[file];
            require('fs').writeFileSync(__dirname + '/' + file, contents, 'utf8');
        });
    }

    cleanupFiles() {
        Object.keys(HelpersTestHelpers.files).map((file) => {
            if (require('fs').existsSync(__dirname + '\\' + file)) {
                try {require('child_process').execSync('rm -f "' + __dirname + '\\' + file + '"');} catch(e) {}
            }
        });
        if (require('fs').existsSync(__dirname + '\\.test')) {
            try {
                require('child_process').execSync('rmdir "' + __dirname + '\\.test"');
            } catch(e) {}
        }
    }
}

HelpersTestHelpers.files = {
    '.test/bundlefile1.js': `
module.exports = 'foobar';
`,
    '.test/bundlefile2.js': `
module.exports = 'foobarius';
`,
    '.test/bundlefile3.js': `
module.exports = 'foobaz';
`,
    '.test/bundlefile4.js': `
module.exports = 'foobat';
`,
    '.test/bundlefile5.js': `
module.exports = {};
`,
    '.test/existingChild.js': `
require('chalk');
`,
    '.test/preExistingChild.js': `
module.exports = () => {require('chalk');}
`
};
    HelpersTestHelpers.files['.test/simpleparentrequire.js'] = `
module.exports = Object.assign({}, require(__dirname + '/simplerequiretest.js'));
module.exports.parentOverrideTest = require(__dirname + '/bundlefile2.js');
`;
    HelpersTestHelpers.files['.test/simplerequiretest.js'] = `
// Bundle files should work.
let Require = require(__dirname + '/../../require.class.js');
let _require = new Require();
_require.override(__dirname + '/bundlefile2.js', \`${HelpersTestHelpers.files['.test/bundlefile2.js']}\`);
_require.override(__dirname + '/bundlefile1.js', \`${HelpersTestHelpers.files['.test/bundlefile1.js']}\`);
module.exports.sameLevelImport = require(__dirname + '/bundlefile1.js');
`;
    HelpersTestHelpers.files['.test/childpostimporttest.js']  = `
// Bundle files should work.
let Require = require(__dirname + '/../../require.class.js');
let _require = new Require();
_require.override(__dirname + '/bundlefile4.js', \`${HelpersTestHelpers.files['.test/bundlefile4.js']}\`);
module.exports.childLevelPostImport = require(__dirname + '/childafterloadrequiretest.js');
`;
    HelpersTestHelpers.files['.test/childpreimporttest.js'] = `
module.exports.childPriorLoadRequireFn = require(__dirname + '/childpriorloadrequiretest.js');
// Bundle files should work.
let Require = require(__dirname + '/../../require.class.js');
let _require = new Require();
_require.override(__dirname + '/bundlefile3.js', \`${HelpersTestHelpers.files['.test/bundlefile3.js']}\`);
module.exports.childLevelPreImport = module.exports.childPriorLoadRequireFn();
`;
    HelpersTestHelpers.files['.test/childpriorloadrequiretest.js'] = `
module.exports = () => {
    return require(__dirname + '/bundlefile3.js');
}
`;
    HelpersTestHelpers.files['.test/childafterloadrequiretest.js'] = `
module.exports = require(__dirname + '/bundlefile4.js');
`;

module.exports = HelpersTestHelpers;