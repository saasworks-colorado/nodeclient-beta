let Require = require(__dirname + '/../require.class.js');
let TestHelpers = require(__dirname + '/test.helpers.js');
describe('Require', () => {
    beforeEach(() => {
        new Require();
        _require = Require.instance;
    });
    it('Should allow a require override to be specified.', () => {
        _require.override('foobarius', `
module.exports = 'foobar';
`);
        spyOn(_require, 'requireWithOverride').and.callThrough();
        expect(_require.hasOverride('foobarius')).toBe(true);
        let exports = require('foobarius');
        expect(_require.requireWithOverride).toHaveBeenCalled();
        expect(exports).toBe('foobar');
    });

    it('Should allow the require function to be released to its original state.', () => {
        _require.override('foobarius', `
module.exports = 'foobar';
`);
        let exports = require('foobarius');
        expect(exports).toBe('foobar');
        Require.instance.release();
        delete Require.instance;
        expect(function() {
            require('foobarius')
        }).toThrow();
        new Require();
    });

    it('Should allow a require of native modules.', () => {
        _require.override('foobarius', `
module.exports = 'foobar';
`);
        
        expect(function() {
            require('color-name');
        }).not.toThrow();
        expect(function() {
            require('chalk');
        }).not.toThrow();
    });

    it('Should a require override to be specified and native modules ' + 
        'to be loaded.', () => {
            _require.override('foobarius', `
module.exports = 'foobar';
`);
            expect(require('fs')['readFileSync']).not.toBe(undefined);
        });

    it('Should allow a require override to be specified in a child module ' + 
        'and for that override the impact parent modules.', () => {
            Require.instance.release();
            delete Require.instance;
            require('fs').writeFileSync(__dirname + '/foo1.js', `
const Require = require(__dirname + '/../require.class.js');
new Require();
Require.instance.override('foobar fashionista', \`
module.exports = 'fooish';
\`);
`, 'utf8');
            require(__dirname + '/foo1.js');

            let foo = '';
            try {
                foo = require('foobar fashionista');
            } catch(e) {
            }
            expect(foo).toBe('fooish');
            require('fs').unlinkSync(__dirname + '/foo1.js');
        });
    
    it('Should allow an override to propagate into all new child modules.', () => {
        require('fs').writeFileSync(__dirname + '/foo2.js', `
module.exports = require('foobar fashion');
`, 'utf8');
        _require.override('foobar fashion', `
module.exports = 'foobar';
`);
        let foo = require(__dirname + '/foo2.js');
        // expect(foo).toBe('foobar');
        require('fs').unlinkSync(__dirname + '/foo2.js');
    });

    it('Should allow an override to be specified and for that to impact ' + 
        'already loaded child modules.', () => {
        Require.instance.release();
        delete Require.instance;
        require('fs').writeFileSync(__dirname + '/foo3.js', `
module.exports = function() {
    return require('fooishness');
};
`, 'utf8');
        let foo = require(__dirname + '/foo3.js');
        _require = new Require();
        _require.override('fooishness', `
module.exports = 'foobar';
`);
        expect(foo()).toBe('foobar');
        require('fs').unlinkSync(__dirname + '/foo3.js');
        });

    it('Should cache required modules as expected.', () => {
        _require.override('foobarius rex', `
let i = 0;
module.exports = i++;
`);
        let exports = require('foobarius rex');
        expect(exports).toBe(0);
        exports = require('foobarius rex');
        expect(exports).toBe(0);
    });

    it('Should allow cache to programmatically deleted.', () => {
        _require.override('foobarius', `
module.exports = 'foobar';
`);
        require('foobarius');
        expect(_require.getCache('foobarius').exports).toBe('foobar');
        _require.deleteCache('foobarius');
        expect(_require.getCache('foobarius')).toBe(false);
        require(__dirname + '/../foobarius');
        expect(_require.getCache('foobarius').exports).toBe('foobar');
        _require.deleteCache('foobarius');
        expect(_require.getCache('foobarius')).toBe(false);
        let cwd = process.cwd();
        process.chdir(__dirname);
        require('../foobarius');
        process.chdir(cwd);
        expect(_require.getCache('foobarius').exports).toBe('foobar');
        _require.deleteCache('foobarius');
        expect(_require.getCache('foobarius')).toBe(false);
    });

    it('Should handle redirection.', () => {
        _require.override('foobarius', `
module.exports = 'foobar';
`);
        _require.redirect('foo master flex', 'foobarius');
        expect(require('foo master flex')).toBe('foobar');
    });

    it('Should allow CWD to be read, irrespective of it the file override.', () => {
        _require.override(__dirname + '/../tests/foo/bar', `
module.exports = 'foobar';
        `);
        _require.override('tests/foo/baz', `
module.exports = 'foobar';
        `);
        expect(require(__dirname + '/../tests/foo/bar')).toBe('foobar');
        let cwd = process.cwd();
        process.chdir(__dirname);
        expect(require('foo/bar')).toBe('foobar');
        process.chdir(cwd);
        process.chdir(__dirname + '/../');
        expect(function() {require('foo/baz')}).toThrow();
        expect(function() {try {require('tests/foo/baz')} catch(e) {console.log(e)}}).not.toThrow();
        process.chdir(__dirname);
        expect(function() {require('foo/baz')}).not.toThrow();
        process.chdir(cwd);
    });
    
    it('Should allow option to not resolve overrides and redirects to ' + 
        'absolute paths.', () => {
            _require.override('tests/foo/bar', `
    module.exports = 'foobar';
            `, true);
            _require.override('tests/foo/baz', `
    module.exports = 'foobar';
            `, true);
            expect(require(__dirname + '/../node_modules/tests/foo/bar'))
                .toBe('foobar');
            let cwd = process.cwd();
            process.chdir(__dirname);
            expect(require('tests/foo/bar')).toBe('foobar');
            process.chdir(cwd);
            process.chdir(__dirname);
            expect(function() {require('node_modules/tests/foo/baz')}).toThrow();
            expect(function() {require('../node_modules/tests/foo/baz')}).not.toThrow();
            process.chdir(__dirname + '/..');
            expect(function() {require('node_modules/tests/foo/baz')}).not.toThrow();
            process.chdir(cwd);
    });

    describe('File-based Tests', () => {
        beforeEach(() => {
            let helpers = new TestHelpers();
            Require.instance.release();
            helpers.createFiles();
        });
        
        it('Should allow file -> require -> child require override -> parent override.', () => {
            let exports = require(__dirname + '/.test/simpleparentrequire.js');
            expect(exports.parentOverrideTest).toBe('foobarius');
        });
        
        it('Should allow file -> require -> child require override -> parent native.', () => {
            let exports = require(__dirname + '/.test/simpleparentrequire.js');
            expect(function() {
                require('chalk');
            }).not.toThrow();
        });

        it('Should allow file -> require override -> new child require.', () => {
            let exports = require(__dirname + '/.test/childpostimporttest.js');
            expect(exports.childLevelPostImport).toBe('foobat');
        });

        it('Should allow file -> require override -> child native.', () => {
            let exports = require(__dirname + '/.test/childpostimporttest.js');
            expect(function() {
                require(__dirname + '/.test/existingChild.js');
            }).not.toThrow();
        });

        it('Should allow file -> require override -> existing child require.', () => {
            let exports = require(__dirname + '/.test/childpreimporttest.js');
            expect(exports.childLevelPreImport).toBe('foobaz');
        });

        it('Should allow file -> require override -> child native.', () => {
            let fn = require(__dirname + '/.test/preExistingChild.js');
            let exports = require(__dirname + '/.test/childpostimporttest.js');
            expect(function() {
                fn();
            }).not.toThrow();
        });
    
        afterEach(() => {
            let helpers = new TestHelpers();
            helpers.cleanupFiles();
        });
    });

    it('Should handle cycles properly.', () => {
        // This is where A requires B, and B requires C, and C requires A.
        // This is where A requires B, and in a timeout B requires A.
        // This is where A requires B, and B requires A.
        _require.override('a-test', `
exports.foo = () => {return 123;};
exports = require('b-test');
`);
        _require.override('b-test', `
exports = require('a-test').foo();
`);
        expect(_require.hasOverride('a-test')).toBe(true);
        let exports = require('a-test');
        // expect(exports).toBe(123);
        // TODO: This requires Object.defineProperty setter on exports because
        //      exports needs to have the default return property in addition 
        //      other explicitly set properties.
        //      e.g. exports = 'foo';
        //      e.g. exports.foo = 'bar'; // Error without Object.defineProperty
    });

    it('Should properly chain the parent module across files.', () => {
        /**
         * This means given the following reuqire chain A->B->C it should be 
         * testable that C's parent is B and B's parent is A.  This test should
         * be repeated for all contexts of a require override:
         *  1) require override and all parent modules and their children are 
         *      maintained, including requiring the override async from other
         *      disparate trees.
         *  2) require override and all children and their children have proper
         *     parent chaining, including async.
         */
    });

    it('Should allow ES5 require statements, e.g. "./foo.js"', () => {
        /**
         * This should likely first check in the process.cwd() and then as a
         * fallback, look for the package.json and attempt to utilize ./ as a
         * relative path from that package.json file.  This needs to be double
         * checked, however, that it conforms to the require standard.
         * https://nodejs.org/api/modules.html#modules_cycles
         * http://wiki.commonjs.org/wiki/Modules/1.1
         */
    });

    it('Should properly escape backticks in eval.', () => {
        _require.override('foobar-backtick', 'let a = 123; ' +
            'module.exports = `${a}`;');
        expect(_require.hasOverride('foobar-backtick')).toBe(true);
        let exports = require('foobar-backtick');
        expect(exports).toBe('123');
    });

    afterEach(() => {
        Require.instance.release();
        delete Require.instance;
    });
});