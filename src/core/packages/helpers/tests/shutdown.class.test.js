const Shutdown = require(__dirname + '/../shutdown.class.js');
describe('Shutdown', () => {
    it('Should run all shutdown promises.', (done) => {
        let a = 1;
        Shutdown.require(() => {
            a++;
        }, 'step 1');
        Shutdown.start(true).then(() => {
            expect(a).toBe(2);
            Shutdown.require(() => {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                        a++;
                        resolve();
                    }, 1500);
                });
            }, 'step 2');
            let b = 1;
            Shutdown.require(() => {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                        b++;
                        resolve();
                    }, 1500);
                });
            }, 'step 3');
            Shutdown.start(true).then(() => {
                expect(a).toBe(4);
                expect(b).toBe(2);
                done();
            });
        });
    });

    it('Should handle edge case with zero shutdown steps.', (done) => {
        Shutdown.start(true).then(() => {
            done();
        });
    });

    afterEach(() => {
        Shutdown.clear();
    });
});