const Override = require(__dirname + '/../override.class.js');
describe('Override', () => {
    it('Should override a function.', () => {
        spyOn(console, 'log').and.callThrough();
        const obj = new Override('console.log', null, console);
        
        // Just test the default.
        console.log('foobar');
        expect(console.log.calls.count()).toEqual(1);

        let foo = 1;
        let bar = '';
        obj.wrap((log, a, b) => {
            foo++;
            bar += b;
            log(a); // Logs a... e.g. 'foo'
            return 123;
        });
        spyOn(console, 'log').and.callThrough();
        
        // Call it and measure the changes.
        console.log('foo', 'foobar');
        expect(foo).toBe(2);
        expect(bar).toBe('foobar');
        expect(console.log.calls.count()).toEqual(1);

        // Call it a second time.
        console.log('foo', 'ius');
        expect(foo).toBe(3);
        expect(bar).toBe('foobarius');
        expect(console.log.calls.count()).toEqual(2); // Spy is in-tact.

        obj.unwrap();
        
        // Now no changes should occur.
        console.log('foo', ' foobar');
        expect(foo).toBe(3);
        expect(bar).toBe('foobarius');
    });

    it('Should wrap multiple layers.', () => {
        const ret = console.log('');
        expect(ret).toBe(undefined); // Tests the release function.
        spyOn(console, 'log').and.callThrough();
        const obj = new Override('console.log', null, console);
        obj.abc = 123;
        let foo = 1;
        obj.wrap((log, b, c) => {
            foo = c;
            log(b);
        });
        const obj2 = new Override('console.log', null, console);
        expect(obj2.abc).toBe(123);
        obj2.wrap((log) => {
            log('foo', 2);
        });
        console.log();
        expect(foo).toBe(2);
    });

    it('Should reset the function to its original state on-demand.', () => {
    });

    afterEach(() => {
        Override.releaseAll();
    });
});