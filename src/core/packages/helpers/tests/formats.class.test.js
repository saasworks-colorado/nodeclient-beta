const Formats = require(__dirname + '/../formats.class.js');
describe('Formats', () => {
    it('should convert JSON to CSV.', () => {
        let csv = Formats.toCSVfromJSON([
            {
                a: '123"234,4',
                b: '234\nzxc'
            },
            {
                a: '234"345,5',
                b: `345
xcv`
            },
            {
                a: '345"456,6',
                b: '456\ncvb',
                c: 123
            }
        ]);
        let json = Formats.toJSONfromCSV(csv);
        let csv2 = Formats.toCSVfromJSON(json);
    });
});