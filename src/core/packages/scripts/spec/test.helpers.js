
class ScriptsTestHelpers {
    constructor() {
        this.files = require('ncli-core-files').Files;
    }

    createFiles() {
        this.createLocalFiles();
        this.createNodeModulesFiles();
    }

    createLocalFiles() {
        this.files.writeFile(__dirname + '/../scripts/blah.js', `
require('fs').writeFileSync(__dirname + '/../blah.txt', 'blah');
`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../scripts/error-file.js', `
require('fs').writeFileSync(__dirname + '/../****blah.txt', 'blah');
`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../scripts/success-file.js', `
console.log('Easy..');
`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../scripts/hanging-file.js', `
setInterval(() => {
    console.log('Easy..');
    console.log('Easy..');
    console.log('Easy..');
}, 500);
`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../scripts/args-file.js', `
console.log(JSON.stringify(require('minimist')(process.argv)));
`, true, 'utf8', true);
    }

    createNodeModulesFiles() {
        // `foo` package.
        this.files.writeFile(__dirname + '/../node_modules/foo/scripts/foobar.js', `
require('fs').writeFileSync(__dirname + '/../foo.txt', 'foo');
`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../node_modules/foo/bar.js', `
require('fs').writeFileSync(__dirname + '/../bar.txt', 'bar');`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../node_modules/foo/scripts/success-file.js', `
console.log('Easy..');
`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../node_modules/foo/package.json', `{
    "name": "foo", 
    "version": "0.0.1",
    "scripts": {
        "bar": "node bar.js"
    }
}`, true, 'utf8', true);

        // `foobar` package.
        this.files.writeFile(__dirname + '/../node_modules/foobar/scripts/foobar.js', `
require('fs').writeFileSync(__dirname + '/../foobar.txt', 'foobar');
`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../node_modules/foobar/scripts/error-multi.js', `
throw new Error('RAWR');
`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../node_modules/foobar/scripts/success-file.js', `
console.log('Easy..');
`, true, 'utf8', true); 
        this.files.writeFile(__dirname + '/../node_modules/foobar/scripts/error-async.js', `
setTimeout(() => {
    throw new Error('RAWR');
});
`, true, 'utf8', true);
        this.files.writeFile(__dirname + '/../node_modules/foobar/package.json', `{
    "name": "foobar", 
    "version": "0.0.1",
    "scripts": {
        "bar": "node bar.js"
    }
}`, true, 'utf8', true);

        // `bar` package.
    this.files.writeFile(__dirname + '/../node_modules/bar/scripts/foobar.js', `
require('fs').writeFileSync(__dirname + '/../foobar.txt', 'foobar');`, true, 'utf8', true);

this.files.writeFile(__dirname + '/../node_modules/bar/scripts/foobar.js', `
require('fs').writeFileSync(__dirname + '/../bar.txt', 'bar');`, true, 'utf8', true);

        this.files.writeFile(__dirname + '/../node_modules/bar/package.json', `{
    "name": "bar",
    "version": "0.0.1"
}`, true, 'utf8', true);
    }

    cleanupFiles() {
        const fs = require('fs');
        try {fs.unlinkSync(__dirname + '/../blah.txt')} catch(e) {}
        this.files.rrmdir(__dirname + '/../node_modules/foo');
        this.files.rrmdir(__dirname + '/../node_modules/foobar');
        this.files.rrmdir(__dirname + '/../node_modules/blah');
    }
}

module.exports = ScriptsTestHelpers;