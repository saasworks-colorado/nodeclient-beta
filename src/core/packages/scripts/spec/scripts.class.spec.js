const ScriptsTestHelpers = require(__dirname + '/test.helpers.js');
const helpers = new ScriptsTestHelpers();
const Scripts = require(__dirname + '/../scripts.class.js');
const Files = require('ncli-core-files').Files;
const Packages = require('ncli-core-files').Packages;

describe('Scripts', () => {
    let scripts;
    beforeEach(() => {
        helpers.createFiles();
        Packages.clearCache();
        Files.clearCache();
        Scripts.instance = null;
        scripts = new Scripts();
    });

    it('Should correctly parse the CLI for simple and scoped scripts.', () => {
        process.argv[2] = 'foo/foobar';
        process.argv[3] = 'bar/foo*';
        process.argv[4] = 'foo*/bar';
        process.argv[5] = 'foobar*/foobar*';
        process.argv[6] = 'foobar';
        const src = Scripts.parseCLI();
        expect(src.length).toBe(5);
        expect(src[0].script).toBe('foobar');
        expect(src[0].packageName).toBe('foo');
        expect(src[1].script).toBe('foo*');
        expect(src[4].packageName).toBe('*');
    });

    it('Should correctly parse the CLI for passing arguments to scripts.', () => {
        process.argv[2] = 'foo/foobar';
        process.argv[3] = '--';
        process.argv[4] = '--foo=bar';
        process.argv[5] = 'foobar';
        process.argv[6] = '--foo=baz';
        process.argv[7] = '--foobar=baz';
        process.argv[8] = 'fooish/foo';
        const src = Scripts.parseCLI();
        expect(src.length).toBe(3);
        expect(src[0].args.length).toBe(1);
        expect(src[0].args[0]).toBe('--foo=bar');
        expect(src[1].args.length).toBe(2);
        expect(src[1].args[0]).toBe('--foo=baz');
        expect(src[1].args[1]).toBe('--foobar=baz');
        expect(src[2].args.length).toBe(0);
    });

    it('Should correctly identify and grab scripts.', () => {
        const packages = (new Packages()).exec();
        const src = scripts.getScripts();
        expect(src.hasOwnProperty('foo')).toBe(true);
        expect(src.hasOwnProperty('bar')).toBe(true);
        expect(src['foo'].isLocal).toBe(false);
        expect(src['ncli-core-scripts'].isLocal).toBe(true);
        for (let name in packages['foo']['packageJSON']['scripts']) {
            expect(src['foo']['scripts'].hasOwnProperty(name)).toBe(true);
            expect(src['foo']['scripts'][name])
                .toBe(packages['foo']['packageJSON']['scripts'][name]);
        }
        expect(src['foo']['scripts']['foobar'].startsWith('node ')).toBe(true);
    });

    it('Should execute a script call against the correct packages.', () => {
        const packages = (new Packages()).exec();
        let src = scripts.getScriptsFromQuery('fo*', 'fo*');
        src.map(({cmd, dir}) => {
            expect(cmd.includes('fo')).toBe(true);
            expect(packages[dir.split(/\//g).pop()]['name'].includes('fo')).toBe(true);
        });
        expect(src.length).toBe(2);
        expect(src[0]['dir'] === packages['foo']['dir']).toBe(true);
        src = scripts.getScriptsFromQuery('blah', 'ncli-*');
        expect(src.length).toBe(1);
    });

    it('Should execute a local script and appropriately fail or succeed.', async () => {
        let results = await scripts.execute('error-*', '*');
        expect(results.length).toBe(3);
        results.map((res) => {
            expect(res.success).toBe(false);
            expect(res.errors.length > 0).toBe(true);
            expect(res.error.length > 0).toBe(true);
            expect(res.exitCode > 0).toBe(true);
        });
        results = await scripts.execute('success-*');
        expect(results.length).toBe(3);
        results.map((res) => {
            expect(res.success).toBe(true);
            expect(res.errors.length == 0).toBe(true);
            expect(res.error).toBe(false);
            expect(res.exitCode == 0).toBe(true);
        });
    });

    describe('Idle scripts.', () => {
        beforeEach(() => {
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
        });

        it('Should properly end scripts that fail to exit.', async () => {
            scripts.configs['scripts']['timeout'] = 5000;
            const results = await scripts.execute('hanging-*', '*');
            expect(results[0].killedOnIdle).toBe(true);
        });
    })

    fit('Should pass args into a script.', async () => {
        const results = await scripts.execute('args-*', '*', ['--foo=bar', '--bar=foobar', '--baz=baztaz']);
        const args = JSON.parse(results[0].output);
        expect(args['foo']).toBe('bar');
        expect(args['bar']).toBe('foobar');
        expect(args['baz']).toBe('baztaz');
    });

    afterEach(() => {
        helpers.cleanupFiles();
    });
});