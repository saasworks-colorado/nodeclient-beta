const Cache = require('ncli-core-cache');
const chalk = require('chalk');
const {Files} = require('ncli-core-files');
const Filter = require('ncli-core-helpers/filter.class.js');
const Packages = require('ncli-core-files/packages.class.js');
const pidusage = require('pidusage');
const minimist = require('minimist');

/**
 * Script runner reads files across packages from scripts/* path and adds them
 * to active package.json on installation.
 * 
 * @class
 */
class Scripts {
    constructor() {
        if (Scripts.instance) {
            return Scripts.instance;
        }
        Scripts.instance = this;

        this.configs = require('ncli-core-configs');
        this.overrideConfigs();

        // TODO: Read from cache if exists.
        this.packages = (new Packages());

        /**
         * @description Object of script names and file paths, keyed by module.
         * @type {Object<String, Object>}
         */
        this.scripts = this.getScripts();
    }

    /**
     * Given a stream, initiates piped streams that can be written to.
     * @param {Stream} stream 
     * @param {Object} res
     */
    captureScriptOutput(stream, res) {
        const {Writable} = require('stream');
        const stdout = new Writable();
        const stderr = new Writable();
        stream.stdout.pipe(stdout);
        stream.stderr.pipe(stderr);
        stdout._write = ((data, encoding, cb) => {
            res.output += data.toString('utf8');
            cb();
        });
        stderr._write = ((data, encoding, cb) => {
            res.errors.push(data.toString('utf8'));
            cb();
        });
    }

    /**
     * Given a module/script format, executes the script.  If no module is 
     * provided, executes the given script across all modules.  
     * @param {String|RegExp} script The script name to execute.  Wildcards 
     * and regular expressions are acceptable.
     * @param {String|RegExp} packageName The package name to scope execution 
     * to.  Wildcards and regular expressions are acceptable.
     * @param {Array} args The arguments to pass into the script.
     * @return {Promise} A promise that resolves when all executed scripts have
     * completed successfully, and rejects when one or more scripts have failed.
     */
    execute(script, packageName, args) {
        const promises = [];
        const scripts = this.getScriptsFromQuery(script, packageName);
        
        for (let {cmd, dir, src, pkg} of scripts)  {
            const result = this.executeScript(cmd, args, dir).then((res) => {
                if (res.killedOnIdle) {
                    console.info(`${chalk.yellow('Timed Out')} after ${this.configs['scripts']['timeout'] / 1000} seconds when running script for ${chalk.magenta(`${pkg.name}/${src}`)}`);
                } else {
                    console.info(`${chalk.green('Success')} running script for ${chalk.magenta(`${pkg.name}/${src}`)}`);
                }
                return res;
            }).catch((res) => {
                console.error(`Error encountered while running script for ${chalk.magenta(`${pkg.name}/${src}`)}`);
                if (res.errors.length > 0) {
                    console.error(`Printing last known error for ` + 
                        `${chalk.magenta(`${pkg.name}/${src}`)}:\n` + 
                        `${chalk.grey(res.errors[res.errors.length - 1])}`);
                }
                return res;
            });
            promises.push(result);
        }
        return Promise.all(promises);
    }

    /**
     * Executes a script given a specific module path and arguments to pass 
     * into a spawned process.  This uses the `spawn` command and breaks up 
     * all arguments by spaces.  Spawn is preferred over exec because the 
     * output is streamed instead of buffered.
     * @param {String} cmd The path to execute when forking Node.js 
     * process.
     * @param {String|Array} args Arguments to pass into the script.
     * @param {String} dir The directory to execute under.
     * @return {Promise} A promise that resolves when the process correctly 
     * executes and closes. Returned data shape looks like as follows: ```
     * {
     *      exitCode: Number,
     *      error: String?,
     *      output: String,
     *      success: Boolean,
     *      command: {
     *          cmd: String,
     *          args: String,
     *          dir: String
     *      }
     * }
     * ```
     */
    executeScript(cmd, args = [], dir) {
        return new Promise((resolve, reject) => {
            let output, stream;
            cmd = cmd.split(/\s+/g);
            args.splice(0, 0, cmd.slice(1));
            cmd = cmd.shift();
            const res = {
                command: {
                    cmd,
                    args,
                    dir
                },
                error: '',
                errors: [],
                output: ''
            };
            try {
                stream = require('child_process')
                    .spawn(cmd, args, {
                        cwd: dir,
                        stdio: ['pipe', 'pipe', 'pipe'],
                        silent: true
                    });
            } catch(e) {
                res.exitCode = -1;
                res.error = `The script failed to run with the following error: ${e}`;
                res.output = false;
                res.success = false;
                reject(res);
            }

            stream.on('close', (code) => {
                if (code > 0) {
                    res.exitCode = code;
                    res.error = `The script encountered an error and produced a non-zero exit code: ${code}`;
                    res.success = false;
                    reject(res);
                } else {
                    res.exitCode = code;
                    res.error = false;
                    res.success = true;
                    resolve(res);
                }
            }); 
            stream.on('error', (...args) => {
                res.exitCode = -1;
                res.error = `The script encountered an error: ${JSON.stringify(args)}`;
                res.success = false;
                resolve(res);
            });
            if (!this.configs['scripts']['showOutput']) {
                this.captureScriptOutput(stream, res);
            }
            this.monitorForIdleProcess(stream.pid, () => {
                res.killedOnIdle = true;
                res.idleTime = this.configs['scripts']['timeout'];
                stream.kill();
            });
        });
    }

    /**
     * Initiates scripts from the CLI.
     */
    static fromCLI() {
        const scripts = Scripts.parseCLI();
        scripts.map(({script, packageName, args}) => {
            Scripts.instance.execute(script, packageName, args);
        });
    }

    /**
     * Given wildcard enabled and RegExp enabled `package/script` queries, 
     * returns the selected scripts.
     * @param {String|RegExp} scriptName 
     * @param {String|RegExp} packageName 
     * @returns {Array} An array of cmd/dir combinations to execute on the CLI.
     */
    getScriptsFromQuery(scriptName, packageName) {
        let packages;
        const scripts = [];
        if (!packageName) {
            packages = this.packages.exec();
        } else {
            packages = (new Packages());
            packages.filterPackages(packageName);
            packages = packages.exec();
        }
        for (let name in packages) {
            if (this.configs['scripts']['ignoreNodeModules'] && 
                !this.scripts[name].isLocal) {
                continue;
            }
            
            if (!this.scripts[name]) {
                continue;
            }
            const filteredScripts = Filter.exec(Object
                .keys(this.scripts[name]['scripts']), scriptName);
            filteredScripts.map((script) => {
                scripts.push({
                    cmd: this.scripts[name]['scripts'][script],
                    dir: this.scripts[name]['dir'],
                    isLifecycle: Scripts.isLifecycleScript(script),
                    src: script,
                    pkg: packages[name]
                });
            });
        }

        return scripts;
    }
    
    /**
     * Gets all the scripts.  Scripts are sourced both from package.json files 
     * and from the filesystem.  When sourced from the filesystem, the script 
     * command becomes, "node [FILENAME]".  The resulting object includes both
     * the directory to chdir to as well as the script process to run.
     * @todo Evaluate if the script target should be configured at all.
     * @todo Allow the input of a package query object.
     * @returns {Object<String, Object>} Object of script names and file paths,
     * keyed by package.
     */
    getScripts() {
        let scripts = {};
        const packages = this.packages.exec();
        for (let name in packages) {
            const {dir, isLocal, packageJSON} = packages[name];
            try {
                const packageScripts = packageJSON['scripts'];
                for (let script in packageScripts) {
                    if (!scripts[name]) {
                        scripts[name] = {
                            dir,
                            isLocal,
                            name,
                            scripts: {}
                        };
                    }
                    // if (Scripts.isLifecycleScript(script)) {
                        // TODO: Change cwd to match the package location.
                        scripts[name]['scripts'][script] = packageScripts[script];
                    // }
                }
            } catch(e) {
                console.warn(err);
            }
        }

        const files = Files.readFiles('scripts', false, false, false);
        // It's imporant to rewrite abs file paths to `node [REL_PATH]`.
        for (let name in files) {
            if (!packages[name]) {
                continue;
            }
            const {dir, isLocal} = packages[name];
            if (!files[name]['files']['scripts']) {
                continue;
            }
            if (!scripts[name]) {
                scripts[name] = {
                    dir,
                    isLocal,
                    name,
                    scripts: {}
                };
            }
            files[name]['files']['scripts'].map((file) => {
                let filename = require('path').basename(file)
                    .split('.').slice(0, -1);
                // TODO: Verify that adding `+ 1` to the length in fact correct in all use cases.
                // It's a package.json script already..
                if (scripts[name]['scripts'][filename]) {
                    // Make me an array.
                    scripts[name]['scripts'][filename] = [
                        scripts[name]['scripts'][filename],
                        `node ${file.substr(dir.length + 1)}`
                    ];
                } else {
                    scripts[name]['scripts'][filename] = 
                        `node ${file.substr(dir.length + 1)}`;
                }
            });
        }

        return scripts;
    }

    /**
     * Returns true if is a lifecycle script.  If true, usually implies a script
     * is to be added as part of a default behavior.
     */
    static isLifecycleScript(name) {
        name = require('path').parse(name).name;
        switch (name) {
            case 'install':
            case 'preinstall':
            case 'postinstall':
            case 'publish':
            case 'prepublish':
            case 'postpublish':
            case 'pack':
            case 'prepack':
            case 'postpack':
            case 'uninstall':
            case 'preuninstall':
            case 'postuninstall':
            case 'version':
            case 'preversion':
            case 'postversion':
            case 'test':
            case 'pretest':
            case 'posttest':
            case 'start':
            case 'prestart':
            case 'poststart':
            case 'restart':
            case 'prerestart':
            case 'postrestart':
            case 'shrinkwrap':
            case 'preshrinkwrap':
            case 'postshrinkwrap':
                return true;
            default:
                return false;
        }
    }

    /**
     * Monitors a process and calls the provided callback when the process has
     * become idle.
     */
    monitorForIdleProcess(pid, cb, interval = 1000) {
        let lastIdle = (new Date()).getTime();
        const itvId = setInterval(() => {
            pidusage(pid, (err, stats) => {
                if (!err) {
                    if (stats.cpu > 0) {
                        lastIdle = (new Date()).getTime();
                    }
                    if ((new Date()).getTime() - lastIdle > 
                        this.configs['scripts']['timeout']) {
                        cb();
                    }
                } else {
                    clearInterval(itvId);
                }
            });
        }, interval);
        return itvId;
    }

    /**
     * Overrides the configs via the CLI.
     */
    overrideConfigs() {
        const args = minimist(process.argv.slice(2));
        const isTrue = val => {
            return val === 'true' || 
                val === 1 || 
                val === '1' ||
                val === true;
        }
        // If true, only pulls scripts from non-node_modules folders.
        if (args['ignoreNodeModules']) {
            this.configs['scripts']['ignoreNodeModules'] = isTrue(args['ignoreNodeModules']);
        }
        if (args['showOutput']) {
            this.configs['scripts']['showOutput'] = isTrue(args['showOutput']);
        }
    }

    /**
     * Function to parse CLI arguments.  Accepts all formats of calling 
     * scripts as stipulated in the README.  
     */
    static parseCLI() {
        const args = minimist(process.argv.slice(2));
        const scripts = [];

        if (args['_'].length > 0) {
            args['_'].map((token) => {
                if (token.startsWith('--')) {
                    if (scripts.length == 0) {
                        return;
                    }
                    scripts[scripts.length - 1].args.push(token);
                } else {
                    const script = token.split('/');
                    const packageName = script.length > 1 ? script[0] : '*';
                    const scriptName = script.length > 1 ? script[1] : script[0];
                    scripts.push({
                        packageName,
                        script: scriptName,
                        args: []
                    });
                }
            });
        }

        return scripts;
    }
}

new Scripts();
module.exports = Scripts;