const minimist = require('minimist')(process.argv);
const Cache = require(__dirname + '/cache.class.js');
if (!minimist.noCache && !minimist['no-cache']) {
    Cache.read();
}

module.exports = Cache;