const Cache = require(__dirname + '/../cache.class.js');
const Files = require('ncli-core-files').Files;
const Packages = require('ncli-core-files').Packages;
describe('Cache', () => {
    beforeEach(() => {
        Cache.clear(null, true);
    })
    it('Should be able to get and set.', () => {
        expect(Cache.has('foo')).toBe(false);
        Cache.set('foo', 'bar', {inMemory: true});
        expect(Cache.has('foo')).toBe(true);
        expect(Cache.get('foo')).toBe('bar');
    });

    it('Should be able to get metadata for a cache object.', () => {
        expect(Cache.read('foo')).toBe(null);
        Cache.set('foo', 'bar', {persist: false});
        Cache.save();
        expect(Cache.read('foo')).toBe(null);
        Cache.set('foo', 'foobar', {persist: true});
        Cache.save();
        let obj = Cache.read('foo');
        expect(obj).not.toBe(null);
    });
    
    it('Should save the cache to the filesystem.', () => {
        Cache.set('fooish', 'bar');
        let file = Cache.file('fooish');
        expect(require('fs').existsSync(file)).toBe(false);
        Cache.save();
        expect(require('fs').existsSync(file)).toBe(true);
    });

    it('Should remove the cache from memory when saving if inMemory is false.', () => {
        Cache.set('fooish', 'bar', {inMemory: false});
        expect(Cache.objects['fooish'].value).not.toBe('bar');
        expect(Cache.read('fooish').value).toBe('bar');
        expect(Cache.objects['fooish'].value).not.toBe('bar');
        expect(Cache.get('fooish')).toBe('bar');
        expect(Cache.objects['fooish'].value).not.toBe('bar');
    });

    it('Should store caches too large to the filesystem when cache invalidated.', () => {
        Cache.configs['cache']['memoryThreshold'] = 4;
        Cache.set('fooish', 'bar');
        expect(Cache.objects['fooish'].value).toBe('bar');
        Cache.clear(null, true);
        Cache.set('fooish', 'foobar');
        expect(Cache.objects['fooish'].value).not.toBe('foobar');
    });

    it('Should store oldest caches to filesystem first when cache invalidated ' + 
        'and cache is exceeding configured memory threshold.', () => {
            Cache.configs['cache']['memoryThreshold'] = 7;
            Cache.set('fooish', 'foobar');
            expect(Cache.objects['fooish'].value).toBe('foobar');
            Cache.set('foobar', 'bar');
            expect(Cache.objects['fooish'].value).not.toBe('foobar');
            expect(Cache.objects['foobar'].value).toBe('bar');
        });

    it('Should read the cache from the filesystem.', () => {
        
    });

    it('Should remove temporary cache files on shutdown.', () => {

    });

    afterEach(() => {
        Cache.clear(null, true);
        Files.clearCache();
        Packages.clearCache();
    });
});