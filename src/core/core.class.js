const chalk = require('chalk');
const childProcess = require('child_process');
const fs = require('fs');
const os = require('os');
const say = require('say');

/**
  * @class This class manages the set of NCLI core packages, bootstraps the 
  * console, configs, scripts and ensures the cache is properly set up.
  */
class NCLICore { 
    constructor() {
        if (NCLICore.instance) {
            return NCLICore.instance;
        }
        NCLICore.instance = this;
        this.argv = require('minimist')(process.argv);
        const d1 = (new Date()).getTime();
        this.bootstrap();
        const d2 = (new Date()).getTime();
        console.info(`NCLICore bootstrapped in ${d2-d1} milliseconds.`);
    }

    /**
     * Bootstrap function requires all packages and attaches them to the core 
     * object.
     * @todo This could use some revision, although it's complex.
     */
    bootstrap() {
        // Override the console first!
        this.coreCache = require(__dirname + '/corecache.class.js');
        process.emit('NCLICore.preBootstrap');
        if (!this.argv['noConsole'] && !this.argv['no-console']) {
            this.console = require('ncli-core-console');
        }
        this.files = require('ncli-core-files').Files;
        process.on('environment', (obj) => {
            obj.value = this.getEnv();
        });
        this.bootstrapLocalConfigs();
        this.bootstrapConfigs();
        this.bootstrapConsole();
        Object.assign(this, {
            cache: require('ncli-core-cache'),
            helpers: require('ncli-core-helpers'),
            packages: require('ncli-core-files').Packages,
            rscandir: require('ncli-core-rscandir'),
        });
        process.emit('NCLICore.bootstrap');
        this.scripts = require('ncli-core-scripts');

        this.packages.getLocalPackages();
        this.packages.getNodeModulesPackages();

        this.bootstrapLocalPackages();
        process.emit('NCLICore.postBootstrap');

        // Bootstrap the core cache on top of the cache.
    }

    /**
     * Bootstraps the configs.  This includes maintaining a local configs tree
     * in addition to a tree for the local package path.
     */
    bootstrapConfigs() {
        this.Configs = require('ncli-core-configs/configs.class.js');
        this.configs = this.getConfigs();
    }

    /**
     * Bootstraps the console according to options.
     */
    bootstrapConsole() {
        if (NCLICore.isTrue(this.argv.verbose)) {
            // Noop.
        } else if (this.getEnv() == 'prod' && 
            (!this.localConfigs['ncli'] || 
            !this.localConfigs['ncli']['verbose'])) {
            // Squelch the core, and all subpackages.
            this.console.addToBlacklist('ncli-core');
            this.console.addToBlacklist('ncli-core-*');
        } else if (!this.localConfigs['ncli'] || 
            !this.localConfigs['ncli']['verbose']) {
                // Squelch the subpackages.
            this.console.addToBlacklist('ncli-core-*');
        }
    }

    /**
     * Bootstraps some external path.
     */
    bootstrapExternal(path) {
        const cwd = process.cwd();
        process.chdir(path);
        process.emit('NCLICore.preBootstrap');
        this.bootstrapConfigs();
        this.bootstrapConsole();
        process.emit('NCLICore.bootstrap');
        this.packages.getLocalPackages();
        this.packages.getNodeModulesPackages();

        this.bootstrapLocalPackages();
        process.emit('NCLICore.postBootstrap');
        process.chdir(cwd);
    }

    /**
     * Bootstraps all internal configs into a variable name called 
     * `localConfigs`.
     */
    bootstrapLocalConfigs() {
        // TODO: This was used when wanting a clutter-free config.
        const cwd = process.cwd();
        process.chdir(__dirname + '/../../');
        this.localConfigs = this.getConfigs();
        process.chdir(cwd);
    }

    /**
     * This creates `require` overrides for all local packages.
     */
    bootstrapLocalPackages() {
        this.localPackageSymlinks = [];
        if (!fs.existsSync(`${process.cwd()}/node_modules/`)) {
            fs.mkdirSync(`${process.cwd()}/node_modules`);
        }
        // Read all local packages.  
        this.packages.getLocalPackages().map(({dir, isMain, name}) => {
            if (!name) {
                return;
            }
            // Creates a symlink for the local packages.
            const target = `${process.cwd()}/node_modules/${name}`;
            const exists = fs.existsSync(target);
            let isSymlink;
            try {
                isSymlink = fs.lstatSync(target).isSymbolicLink();
            } catch (e) {}
            if (!exists && !isSymlink && !isMain) {
                console.info(`Creating symlink for ${name}`);
                fs.symlinkSync(dir, target, 'dir');
                this.localPackageSymlinks.push(target);
            }
        });
    }

    /**
     * Cleans up symlinks upon exit or on-demand.  This array is populated by 
     * the NCLICore.bootstrapLocalPackages function.
     */
    cleanupLocalPackageSymlinks() {
        this.localPackageSymlinks.map((symlink) => {
            childProcess.execSync(`unlink ${symlink}`);
        });
    }

    /**
     * @todo Should offer parameter to refresh the configs.
     * @returns {Object} The configs object.
     */
    getConfigs(readAll = false) {
        let configs;
        if (readAll) {
            configs = this.Configs.readJSON('configs');
        } else {
            configs = this.Configs.readLocalJSON('configs');
        }
        for (let config in configs) {
            let env;
            let envParts = config.split(/\./g);
            if (envParts.length > 1) {
                env = envParts.pop();
            } else {
                continue;
            }
            const key = envParts.join('.');
            if (env == this.getEnv()) {
                if (!configs[key]) {
                    configs[key] = {};
                }
                configs[key] = this.Configs.deepAssign(configs[key], configs[config]);
            }
        }
        return configs;
    }

    /**
     * @returns {String} The environment which is supplied by the ENV property
     * on startup.
     */
    getEnv() {
        let {env} = this.argv;
        if (env == 'development') {
            env = 'dev';
        } else if (env == 'production') {
            env = 'prod';
        } else if (env == 'stage') {
            env = 'staging';
        }
        if (!env) {
            let mainFile = this.files.normalize(require.main.filename); 
            // TODO: Add a check if using mocha or other test runners as well.
            if (mainFile.endsWith('jasmine.js')) {
                // We're in a test runner.
                let globalDir = 
                    childProcess.execSync('npm config get prefix').toString('utf8').trim();
                globalDir = this.files.normalize(globalDir);
                if (mainFile.includes(globalDir)) {
                    return 'test';
                }
            }
        }
        return env || 'dev';
    }
    
    /**
     * Gets a suffix for use with file inclusions.
     */
    getEnvSuffix() {
        const env = this.getEnv();
        return env == 'prod' ? '' : `-${env}`;
    }

    /**
     * @param {String} fromFolder The folder from which to get file listings.
     * @param {Boolean} byPackage If true, 
     * @returns {Array|Object}
     */
    getFiles(fromFolder, byPackage = false, flushCache = false, noRead = false) {
        const files = this.files
            .readLocalFiles(fromFolder, noRead, false, !byPackage);
        if (byPackage) {
            const filesByPackage = {};
            for (let pkg in files) {
                if (files[pkg]['files'].hasOwnProperty(fromFolder) && 
                    files[pkg]['files'][fromFolder].length > 0) {
                    filesByPackage[pkg] = files[pkg]['files'][fromFolder];
                }
            }
            return filesByPackage;
        } else {
            return files;
        }
    }

    /**
     * @param {String} fromFolder The folder from which to get file contents.
     * @returns {Object} An object containing filenames and their associated 
     * content.  If `byPackage` is true, then structures returned object by 
     * package name, then filename.
     */
    getFileContents(fromFolder, byPackage = false, flushCache = false) {
        const files = this.getFiles(fromFolder, byPackage, flushCache);
        if (!byPackage) {
            const fileContents = {};
            files.map((file) => {
                try {
                    const contents = fs.readFileSync(file, 'utf8');
                    fileContents[file] = contents;
                } catch(e) {}
            });
            return fileContents;
        } else {
            const fileContentsByPackage = {};
            for (let pkg in files) {
                files[pkg].map((file) => {
                    try {
                        const contents = fs.readFileSync(file, 'utf8');
                        if (!fileContentsByPackage[pkg]) {
                            fileContentsByPackage[pkg] = {};
                        }
                        fileContentsByPackage[pkg][file] = contents;
                    } catch(e) {}
                });
            }
            return fileContentsByPackage;
        }
    }

    /**
     * @param {String} fromFolder The folder from which to get JSON.
     */
    getJSON(fromFolder) {
        return this.Configs.readLocalJSON(fromFolder);
    }

    /**
     * Gets all local packages.  Forces a refresh of the cache.
     */
    getPackages() {
        return this.packages.getLocalPackages();
    }

    /**
     * @returns {Object} The configs object.
     */
    getScripts() {
        const scripts = this.scripts.instance.getScripts();
        const localScripts = {};
        for (let pkg in scripts) {
            if (!scripts[pkg].isLocal) {
                continue;
            }
            if (Object.keys(scripts[pkg]['scripts']).length == 0) {
                continue;
            }
            localScripts[pkg] = scripts[pkg];
        }
        return localScripts;
    }

    /**
     * @returns {Boolean} Coerces CLI argument into boolean.
     */
    static isTrue(val) {
        return val === 'true' || 
            val === 1 || 
            val === '1' ||
            val === true;
    }

    /**
     * Allows the CLI to override configs for `--configs.<STORE>.<KEY>=<VALUE>`.
     * This does not save anything to the configs.
     */
    parseCLIConfigs() {
        
    }

    /**
     * @param {String} fromFolder The folder from which to require files.
     * @param {Function} fn The callback function called on each require.  
     *  Function signature is (exports, packageName, file).  Return value
     *  is ignored in callback function.
     */
    requireFiles(fromFolder, fn, errFn) {
        const files = this.getFiles(fromFolder, true);
        for (let pkg in files) {
            files[pkg].map((file) => {
                try {
                    const exports = require(file);
                    fn(exports, pkg, file);
                } catch(e) {
                    errFn(e, pkg, file);
                }
            });
        }
    }

    /**
     * @returns {Promise} A promise that resolves once words have been spoken.
     */
    saySomething(textToSpeech) {
        let voice;
        if (process.platform == 'win32') {
            voice = 'Microsoft Zira Desktop';
        }
        return new Promise((res) => {
            say.speak(textToSpeech, voice, 
                1.125, res);
        });
    }
}

const ncliCore = (new NCLICore());
ncliCore.helpers.Shutdown.require(() => {
    ncliCore.cleanupLocalPackageSymlinks();
}, 'Cleaning up symlinks.');

module.exports = ncliCore;
