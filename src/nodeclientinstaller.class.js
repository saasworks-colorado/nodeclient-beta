const chalk = require('chalk');
const childProcess = require('child_process');
const fs = require('fs');
const os = require('os');
const NCLICore = require(__dirname + '/core/core.class.js');

/**
 * @class
 * This class is responsible for installing the NodeClient packaging onto the
 * OS.  Two things happen with this class.  First, to resolve odd NPM/yarn 
 * errors, each submodule is installed in proper succession of its dependencies.
 * 
 * Following this, each package is linked into the HOME folder.  Next, each 
 * of the submodules described 
 */
class NCLIInstaller {
    /**
     * Checks if the install already exists or not.  This includes if there 
     * is a lower/higher version already installed.  This might abort if the 
     * install is already occurring from the global folder, i.e. process.cwd()
     * is the global folder.  This includes information if the installation is 
     * a development build or a production build. 
     */
    static checkInstall() {
        const global = NCLIInstaller.getGlobalNodeModulesFolder();
        let isInInstallFolder = false;
        if (process.cwd().includes(global)) {
            // We're in the global folder already.
            isInInstallFolder = true;
        }

        let exists
        if (fs.existsSync(global + '/ncli')) {

        }

        return {
            isInInstallFolder
        }
    }

    /**
     * Gets the home folder which differs across OS.
     */
    static getHomeFolder() {
        if (process.platform == 'win32') {
            return (new Buffer(cp.execSync(`echo %HOMEPATH%`))
                .toString('utf8'))
                .trim()
                .replace(/\\/g, '/');
        } else {
            // Linux or Mac.
            return `${process.env['HOME']}/.node_modules`;
        }
    }

    /**
     * @returns {String} The global node_modules folder.
     */
    static getGlobalNodeModulesFolder() {
        if (os.platform() == 'win32') {
            return NCLIInstaller.getNPMFolder() + '/node_modules';
        } else if (os.platform() == 'linux' || true /* TODO: revise */) {
            return NCLIInstaller.getNPMFolder() + '/lib/node_modules';
        }
    }

    /**
     * @returns {String} The NPM global folder.
     */
    static getNPMFolder() {
        if (NCLIInstaller.npmFolder) {
            return NCLIInstaller.npmFolder;
        }
        NCLIInstaller.npmFolder = require('child_process').execSync('npm config get prefix')
            .toString('utf8').trim()
            .replace(/\\/g, '/');

        return NCLIInstaller.npmFolder;
    }

    /**
     * Installs the NCLI.  
     */
    static async install() {
        await new Promise((res) => {
            say.speak('Installing NCLI locally.', 
                'Microsoft Zira Desktop', 
                1.125, res);
        });
    }

    /**
     * From a local context, installs the core software.
     */
    static async localInstallCore(overwrite = false) {
        const dest = this.getNPMFolder();
        const {development} = NCLICore.configs;
        console.info('Installing NCLI Locally.');
        if (overwrite) {
            this.uninstallCLI();
        }

        try {
            fs.symlinkSync(__dirname + '/../bin/ncli', `${dest}/ncli`);
            fs.chmodSync(`${dest}/ncli`, 755);
        } catch(e) {}

        try {
            fs.symlinkSync(__dirname + '/../bin/ncli.cmd', dest + '/ncli.cmd');
            fs.chmodSync(`${dest}/ncli.cmd`, 755);
        } catch(e) {}

        try {
            fs.symlinkSync(__dirname + '/../', dest + '/node_modules/ncli', 'dir');
            fs.chmodSync(`${dest}/node_modules/ncli`, 755);
        } catch(e) {}

        console.info('Done.');
    }

    /**
     * Installs the CLI file.
     */
    static installCLI() {
        const src = NCLIInstaller.files.normalize(__dirname + '/bin/');
        require('fs').chmodSync(`${dest}/ncli`, 715);
        require('fs').chmodSync(`${dest}/ncli.cmd`, 715);
    }

    /**
     * Installs a local module given a directory of that module.
     * @param {String} dir The directory in which to find the module.
     */
    static installLocalModule(dir) { 
        return new Promise((resolve, reject) => {
            const cwd = process.cwd();
            process.chdir(dir);
            try {
                childProcess.exec(`npm i ${dir} --save`).on('close', resolve);
            } catch(e) {}
            process.chdir(cwd);
        });
    }

    /**
     * Checks if NodeClient is installed.
     * @param {String} env If provided, checks for the specific env.
     */
    static isInstalled(env = 'prod') {
        const home = this.getHomeFolder();
        const suffix = env == 'prod' ? '' : `-${env}`;
        return fs.existsSync(`${home}/ncli${suffix}`);
    }

    /**
     * Removes a global package, 
     */
    static removeGlobalPackage() {

    }

    /**
     * Safe install removes the package-lock file, then node_modules, then 
     * reinstalls.
     * @param {String} dir The directory in which to find the module.
     */
    static safeInstall(dir) {
        const cwd = process.cwd();
        process.chdir(dir);
        console.log(chalk.cyan('Safely Installing'), dir);
        try {fs.unlinkSync('./package-lock.json');} catch(e) {}
        try {
            childProcess.execSync('rm -rf node_modules', 
                {encoding: 'utf8', stdio: 'ignore'});
        } catch(e) {
            console.log(chalk.red('Failure Occurred'), dir);
        }
        try {
            childProcess.execSync('npm i', {encoding: 'utf8', stdio: 'ignore'});
        } catch(e) {
            console.log(chalk.red('Failure Occurred'), dir);
        }
        process.chdir(cwd);
    }

    /**
     * This forces the CLI and the `ncli` package to be uninstalled.
     */
    static uninstallCLI(){
        const dest = this.getNPMFolder();
        const remove = (folderOrFile) => {
            try {
                if (fs.lstatSync(folderOrFile).isSymbolicLink()) {
                    childProcess.execSync(`rm -f ${folderOrFile}`);
                }
            } catch(e) {}
            if (fs.existsSync(`${folderOrFile}`)) {
                fs.unlinkSync(`${folderOrFile}`);
            }
        };

        remove(`${dest}/ncli`);
        remove(`${dest}/ncli.cmd`);
        remove(`${dest}/node_modules/ncli`);
    }
}

module.exports = NCLIInstaller;