/**
 * The main class for NodeClient.
 * @class
 */
class NCLI {
    constructor() {
        if (NCLI.instance) {
            return NCLI.instance;
        }
        NCLI.instance = this;
        if (!this.checkInstall()) {
            this.install();
        }
    }
}