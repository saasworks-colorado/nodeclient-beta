/**
 * @fileoverview This file launches the NCLI server.
 */
const ncli = require('ncli');
ncli.server.node.start();