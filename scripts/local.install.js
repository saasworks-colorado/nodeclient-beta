/**
 * @fileoverview
 * This file installs the NodeClient software locally.  This avoids the need
 * to push to the public GitHub/NPM repository.
 */
const NodeClientInstaller = 
    require(__dirname + '/../src/nodeclientinstaller.class.js');
const configs = require(__dirname + '/../src/core/packages/configs');
process.chdir(__dirname + '/../');

configs['development']['packages'].map((dir) => {
    NodeClientInstaller.symlinkModule(dir, 'dev');
});