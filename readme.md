<span style="display: block; text-align: center">![Nodeclient](media/nodeclient-full.png)</span>

# NCLI

nodeclient (NCLI) assists developers with managing multiple Node.js packages in 
an application ecosystem to run web servers with advanced functionality.  

## Installation

```
 $ npm i -g nodeclient
```

### Note: Global Install Forced
Note that if you do not install Nodeclient globally, Nodeclient will move itself
to your global folder on first run.  On first run, Nodeclient will copy itself
to your `$(npm config get prefix)` OS-specific folder:

* Mac/Linux: `$(npm config get prefix)/lib/node_modules`
* Windows: `$(npm config get prefix)/node_modules`

This choice was made because Nodeclient includes a `bin` script.

### Note: One-Time Install
Nodeclient links itself in your `~/.node_modules` folder such that you can
always `require('nodeclient')` from within your code.  Note that 
`~/.node_modules` is in the lookup paths of `require('module').globalPaths`.

This choice was made because Nodeclient takes more than a minute to install.  
This way, you only need to install Nodeclient once.

### Windows Pre-Requisite
You'll need to install the [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10).  This allows you
to run bash in a Windows environment, and it's used by NCLI during install.

## License

Please view the license file for more information.  NCLI is subject to all terms
laid out in its license file.